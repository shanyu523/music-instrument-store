-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2017 at 07:02 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `youde`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `Admin_Id` int(10) NOT NULL,
  `Admin_Username` varchar(10) NOT NULL,
  `Admin_Password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Admin_Id`, `Admin_Username`, `Admin_Password`) VALUES
(1, 'admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `agent`
--

CREATE TABLE `agent` (
  `Agent_Id` int(10) NOT NULL,
  `Agent_UserName` varchar(100) DEFAULT NULL,
  `Agent_Password` varchar(100) DEFAULT NULL,
  `Agent_Name` varchar(100) DEFAULT NULL,
  `Agent_IC` bigint(100) DEFAULT NULL,
  `Agent_Birthday` date DEFAULT NULL,
  `Agent_Phonenumber` varchar(15) DEFAULT NULL,
  `Agent_Address` varchar(100) DEFAULT NULL,
  `Agent_City` varchar(20) NOT NULL,
  `Agent_Postcode` int(20) DEFAULT NULL,
  `Agent_State` varchar(10) DEFAULT NULL,
  `Agent_Email` varchar(100) DEFAULT NULL,
  `Agent_Package` varchar(20) DEFAULT NULL,
  `Agent_Status` varchar(15) DEFAULT NULL,
  `Agent_Photo` varchar(100) DEFAULT NULL,
  `Sponsor_ID` int(20) DEFAULT NULL,
  `Sponsor_left` int(20) DEFAULT NULL,
  `Sponsor_right` int(20) DEFAULT NULL,
  `Sponsor_left_count` bigint(100) DEFAULT NULL,
  `Sponsor_right_count` bigint(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agent`
--

INSERT INTO `agent` (`Agent_Id`, `Agent_UserName`, `Agent_Password`, `Agent_Name`, `Agent_IC`, `Agent_Birthday`, `Agent_Phonenumber`, `Agent_Address`, `Agent_City`, `Agent_Postcode`, `Agent_State`, `Agent_Email`, `Agent_Package`, `Agent_Status`, `Agent_Photo`, `Sponsor_ID`, `Sponsor_left`, `Sponsor_right`, `Sponsor_left_count`, `Sponsor_right_count`) VALUES
(1, 'YSY', 'abc', 'yap shan yu', 123456789, '0000-00-00', '123456789', 'asdfghjk12345', 'johor', 81300, 'johor', 'jkdafjkds@gmail.com', '300', 'Pending', '', 1, 2, 3, 2, 1),
(2, 'CBS', 'e85793785cf65ae5eb21203c99845d6c', 'Chua BIng Seng', 604014896, '2000-06-04', '011612358', '6, Jalan Daliah 12, Taman daliah', 'johor', 82500, 'Johor', 'CBS@gmail.com', '6000', 'Approve', NULL, 1, 9, NULL, NULL, NULL),
(3, 'SBS', 'd47bd33854a15e8ec9e0819416db2507', 'sbs', 123456789012, '1994-03-16', '41234123412', 'afsdfsadf', 'johor', 81300, 'Johor', 'afsd@gmail', '6000', 'Approve', NULL, 1, NULL, NULL, NULL, NULL),
(9, 'Test8', '62c8ad0a15d9d1ca38d5dee762a16e01', 'asdf', 123456789012, '2017-12-06', '12341234123', 'asdfasdfg213', 'aasdf', 12344, 'Kedah', 'asdfasd@gmail.com', '6000', 'Approve', NULL, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `Brand_ID` int(50) NOT NULL,
  `Brand_Name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `Category_ID` int(100) NOT NULL,
  `Categoty_Name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`Category_ID`, `Categoty_Name`) VALUES
(1, 'Cosmetic'),
(2, 'Supplement');

-- --------------------------------------------------------

--
-- Table structure for table `cus_order`
--

CREATE TABLE `cus_order` (
  `Order_ID` int(100) NOT NULL,
  `Order_Date` date DEFAULT NULL,
  `Order_Status` varchar(100) DEFAULT NULL,
  `Agent_Id` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cus_order`
--

INSERT INTO `cus_order` (`Order_ID`, `Order_Date`, `Order_Status`, `Agent_Id`) VALUES
(1, '2017-12-25', 'Pending', 2),
(2, '2017-12-23', 'Approve', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ewallet`
--

CREATE TABLE `ewallet` (
  `Ewallet_Id` int(50) NOT NULL,
  `Ewallet_Password` bigint(100) DEFAULT NULL,
  `Ewallet_Registration` int(100) DEFAULT NULL,
  `Ewallet_Online` int(100) DEFAULT NULL,
  `Ewallet_Withdraw` int(100) DEFAULT NULL,
  `Sponsor_Bonus` int(50) DEFAULT NULL,
  `Sponsor_Pairing` int(50) DEFAULT NULL,
  `Agent_Id` int(50) DEFAULT NULL,
  `Id` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ewallet`
--

INSERT INTO `ewallet` (`Ewallet_Id`, `Ewallet_Password`, `Ewallet_Registration`, `Ewallet_Online`, `Ewallet_Withdraw`, `Sponsor_Bonus`, `Sponsor_Pairing`, `Agent_Id`, `Id`) VALUES
(1, NULL, 5000, 5000, 0, 300, 500, 1, 1),
(2, NULL, 6000, 4000, 0, 400, 0, 2, 1),
(204897, 503557, NULL, 6000, NULL, NULL, NULL, 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ewallet_transfer`
--

CREATE TABLE `ewallet_transfer` (
  `TID` int(10) NOT NULL,
  `sender_ID` int(15) DEFAULT NULL,
  `s_Amount` bigint(100) DEFAULT NULL,
  `receiver_ID` int(15) DEFAULT NULL,
  `r_Amount` bigint(100) DEFAULT NULL,
  `detail` varchar(100) DEFAULT NULL,
  `Debit_Time` date DEFAULT NULL,
  `TStatus` varchar(10) DEFAULT NULL,
  `Ewallet_Id` int(50) DEFAULT NULL,
  `Agent_Id` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `Images_ID` int(100) NOT NULL,
  `Images_1` varchar(200) DEFAULT NULL,
  `Images_2` varchar(200) DEFAULT NULL,
  `Images_3` varchar(200) DEFAULT NULL,
  `Product_ID` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`Images_ID`, `Images_1`, `Images_2`, `Images_3`, `Product_ID`) VALUES
(14, 'avatar1.jpg', 'avatar1_small.jpg', 'sample-img-1.jpg', NULL),
(15, 'avatar1.jpg', 'avatar1_small.jpg', 'sample-img-1.jpg', NULL),
(16, '', '', '', NULL),
(17, 'avatar-mini.jpg', 'avatar-mini2.jpg', 'line-icon-hover.png', NULL),
(18, 'profile-avatar.jpg', 'Germany.png', 'Spain.png', NULL),
(19, 'avatar1.jpg', 'avatar-mini3.jpg', 'profile-avatar.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE `orderdetail` (
  `Orderdetail_ID` int(100) NOT NULL,
  `Order_ID` int(100) DEFAULT NULL,
  `Order_Quantity` int(100) DEFAULT NULL,
  `Order_Price` int(100) DEFAULT NULL,
  `Product_ID` int(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orderdetail`
--

INSERT INTO `orderdetail` (`Orderdetail_ID`, `Order_ID`, `Order_Quantity`, `Order_Price`, `Product_ID`) VALUES
(1, 1, 2, 2000, 14),
(2, 2, 3, 2000, 16);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `Product_ID` int(100) NOT NULL,
  `Product_Name` varchar(100) NOT NULL,
  `Product_Point` int(50) NOT NULL,
  `Product_Discount` int(20) NOT NULL,
  `Product_Description` text NOT NULL,
  `Product_Quantity` int(30) NOT NULL,
  `Product_Status` varchar(30) NOT NULL,
  `Product_UpdateDate` date NOT NULL,
  `Product_AddDate` date NOT NULL,
  `Category_ID` int(20) NOT NULL,
  `Subcategory_ID` int(20) NOT NULL,
  `Brand_ID` int(20) DEFAULT NULL,
  `Images_ID` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`Product_ID`, `Product_Name`, `Product_Point`, `Product_Discount`, `Product_Description`, `Product_Quantity`, `Product_Status`, `Product_UpdateDate`, `Product_AddDate`, `Category_ID`, `Subcategory_ID`, `Brand_ID`, `Images_ID`) VALUES
(14, 'Panda cosmic', 2000, 20, 'Panda Cosmetic to Cure dark circle.', 1000, 'Available', '0000-00-00', '2017-12-24', 1, 1, NULL, 15),
(16, 'Vitamin E', 1500, 50, 'Provide your daily need.', 1000, 'Available', '0000-00-00', '2017-12-24', 2, 2, NULL, 17),
(17, 'Vitamin C', 1500, 50, 'Provide the vitamin that you need.', 100, 'Sales', '0000-00-00', '2017-12-24', 1, 1, NULL, 18),
(18, 'asdfa', 12312, 10, 'lol', 100, 'Available', '0000-00-00', '2017-12-25', 1, 1, NULL, 19);

-- --------------------------------------------------------

--
-- Table structure for table `sponsor`
--

CREATE TABLE `sponsor` (
  `Id` int(100) NOT NULL,
  `Sponsor_Id` int(100) NOT NULL,
  `Sponsor_Name` varchar(100) NOT NULL,
  `Sponsor_Left` int(100) DEFAULT NULL,
  `Sponsor_Right` int(100) DEFAULT NULL,
  `Agent_LId` int(50) DEFAULT NULL,
  `Agent_RId` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsor`
--

INSERT INTO `sponsor` (`Id`, `Sponsor_Id`, `Sponsor_Name`, `Sponsor_Left`, `Sponsor_Right`, `Agent_LId`, `Agent_RId`) VALUES
(1, 1, 'YSY', NULL, NULL, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `Subcategory_ID` int(100) NOT NULL,
  `Subcategory_Name` varchar(200) NOT NULL,
  `Category_ID` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`Subcategory_ID`, `Subcategory_Name`, `Category_ID`) VALUES
(1, 'Cosmetic', 1),
(2, 'Supplement', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`Agent_Id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`Brand_ID`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`Category_ID`);

--
-- Indexes for table `cus_order`
--
ALTER TABLE `cus_order`
  ADD PRIMARY KEY (`Order_ID`),
  ADD KEY `Agent_Id` (`Agent_Id`);

--
-- Indexes for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD PRIMARY KEY (`Ewallet_Id`),
  ADD KEY `Agent_Id` (`Agent_Id`),
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `ewallet_transfer`
--
ALTER TABLE `ewallet_transfer`
  ADD PRIMARY KEY (`TID`),
  ADD KEY `Agent_Id` (`Agent_Id`),
  ADD KEY `EWallet_Id` (`Ewallet_Id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`Images_ID`);

--
-- Indexes for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD PRIMARY KEY (`Orderdetail_ID`),
  ADD KEY `Product_ID` (`Product_ID`),
  ADD KEY `Order_ID` (`Order_ID`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`Product_ID`),
  ADD KEY `Brand_ID` (`Brand_ID`),
  ADD KEY `Category_ID` (`Category_ID`),
  ADD KEY `Images_ID` (`Images_ID`),
  ADD KEY `Subcategory_ID` (`Subcategory_ID`);

--
-- Indexes for table `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Agent_Id` (`Agent_LId`),
  ADD KEY `Agent_RId` (`Agent_RId`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`Subcategory_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent`
--
ALTER TABLE `agent`
  MODIFY `Agent_Id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `Brand_ID` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `Category_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cus_order`
--
ALTER TABLE `cus_order`
  MODIFY `Order_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ewallet`
--
ALTER TABLE `ewallet`
  MODIFY `Ewallet_Id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=204898;

--
-- AUTO_INCREMENT for table `ewallet_transfer`
--
ALTER TABLE `ewallet_transfer`
  MODIFY `TID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `Images_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `orderdetail`
--
ALTER TABLE `orderdetail`
  MODIFY `Orderdetail_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `Product_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sponsor`
--
ALTER TABLE `sponsor`
  MODIFY `Id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `Subcategory_ID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cus_order`
--
ALTER TABLE `cus_order`
  ADD CONSTRAINT `cus_order_ibfk_1` FOREIGN KEY (`Agent_Id`) REFERENCES `agent` (`Agent_Id`);

--
-- Constraints for table `ewallet`
--
ALTER TABLE `ewallet`
  ADD CONSTRAINT `ewallet_ibfk_1` FOREIGN KEY (`Agent_Id`) REFERENCES `agent` (`Agent_Id`),
  ADD CONSTRAINT `ewallet_ibfk_2` FOREIGN KEY (`Id`) REFERENCES `sponsor` (`Id`);

--
-- Constraints for table `ewallet_transfer`
--
ALTER TABLE `ewallet_transfer`
  ADD CONSTRAINT `ewallet_transfer_ibfk_1` FOREIGN KEY (`Agent_Id`) REFERENCES `agent` (`Agent_Id`),
  ADD CONSTRAINT `ewallet_transfer_ibfk_2` FOREIGN KEY (`EWallet_Id`) REFERENCES `ewallet` (`Ewallet_Id`);

--
-- Constraints for table `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `orderdetail_ibfk_1` FOREIGN KEY (`Product_ID`) REFERENCES `product` (`Product_ID`),
  ADD CONSTRAINT `orderdetail_ibfk_2` FOREIGN KEY (`Order_ID`) REFERENCES `cus_order` (`Order_ID`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`Brand_ID`) REFERENCES `brand` (`Brand_ID`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`Category_ID`) REFERENCES `category` (`Category_ID`),
  ADD CONSTRAINT `product_ibfk_3` FOREIGN KEY (`Images_ID`) REFERENCES `image` (`Images_ID`),
  ADD CONSTRAINT `product_ibfk_4` FOREIGN KEY (`Subcategory_ID`) REFERENCES `subcategory` (`Subcategory_ID`);

--
-- Constraints for table `sponsor`
--
ALTER TABLE `sponsor`
  ADD CONSTRAINT `sponsor_ibfk_1` FOREIGN KEY (`Agent_LId`) REFERENCES `agent` (`Agent_Id`),
  ADD CONSTRAINT `sponsor_ibfk_2` FOREIGN KEY (`Agent_RId`) REFERENCES `agent` (`Agent_Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
