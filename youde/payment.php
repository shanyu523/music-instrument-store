	<?php
		session_start();
	include('connection.php');	
    	
	if(!isset($_SESSION['sess_id']))
	{
		header("location:login.php");
	}

	$un=$_SESSION["uname"];
	$cn=$_SESSION["cnum"];
	$ad=$_SESSION["add"];
	$cyy=$_SESSION["cy"];
	$pcc=$_SESSION["pc"];
	$stt=$_SESSION["st"];
		

?>


<!doctype html>
<html>
<head>
<title>Payment</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/nav.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.js"></script>
<script src="js/additional-methods.min.js"></script>
 <script src="//code.jquery.com/jquery-1.10.2.js"></script>
<style type="text/css">

#content
{
  margin:20px;
}
.payy
{
   padding-left:250px;	
}
label
{
font-family: Georgia, "Times New Roman", Times, serif;
  text-align: right;
    clear: both;
    float:left;
    margin-right:10px;
}
.text
{
   margin:20px;
}
input[type="text"]
{
   border:1px solid #d1d1d1;
   border-radius:4px;
   height:32px;
}
select
{
  height:30px;
  border:1px solid #d1d1d1;
   border-radius:4px;
}
option {
    font-weight: normal;
    display: block;
    padding: 0px 2px 1px;
    white-space: pre;
    min-height: 1.2em;
}
#payment
{
   width:800px;
   height:600px;
   border:1px solid #dde2e4;
  float:left;
  margin-top:20px;
}
#h
{
    border-bottom:1px solid #dde2e4;
	height:50px;
	width:800px;
	font-size:18pt;
    font-weight:700;
	padding:2px;
}
#py
{
	width:500px;
	height:360px;
    margin:auto;
}
.t
{
  margin:35px;   
}
#button
{
   float:right;
   margin-right:25px;
}
#button input[type="submit"]
{
 border-radius:5px;
 border:1px solid #f36b2d;
 background-color:#f36b2d;
 text-align: center;
 color:white;
 font-weight:700;
 height:40px;
}
#yy input[type="button"]
{
 border-radius:5px;
 border:1px solid #f36b2d;
 background-color:#f36b2d;
 text-align: center;
 color:white;
 font-weight:700;
 height:40px;
}
label.error {
  color:red;}
</style>
<script type="text/javascript">
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
</script>
</head>
<body>
   <div id="wrapper">
       <?php include_once("header.php");?>
	     <div id="content">
		   <div class="payy">
		      <div id="payment">
			         <div id="h">
					
                         <p>Payment Details
						 <span style="float:right;margin-right:10px;"><img src="images/card.png"  width="120px" height="30px"> </span>
					      </p>
                     </div>	
                        <div id="py">	
					    <form name="payment" action="" method="post" id="pay">  
						    <div class="t">
							    <div id="card">
							      <label style="margin-top:8px">Type of Card:</label>
							     <p style="margin-left:130px" >
							       <select name="tp">
								   <option value="default">SELECT CARD </option>
								   <option value="mastercard">MASTERCARD</option>
								   <option value="visa">VISA</option>
								 
								   </select>
							   </p>
							   </div>
							</div>
                             <div class="t">							  
                                <label>Name of Card:</label>
							    <p style="margin-left:130px;"><input name="cardname" type="text" id="" size="25" alt="visa or master"/></p>
								</div>
				             <div class="t">
                            <label>Number of Card:</label>
							<p><input name="cardnumber" type="text"  size="25" maxlength="16" onkeypress="return isNumber(event)"  /></div></p>
							<div class="t">
                            <label>Expiry Date:</label>
							<p style="margin-left:130px;"><select name="month">
							                             <option value="default">Month</option>
                                                         <option value="01-January">01-January</option>
                                                         <option value="02-February">02-February</option>
                                                         <option value="03-March">03-March</option>
                                                         <option value="04-April">04-April</option>
			                                             <option value="05-May">05-May</option>
			                                             <option value="06-June">06-June</option>
			                                             <option value="07-July">07-July</option>
			                                             <option value="08-August">08-August</option>
										                 <option value="09-September">09-September</option>
			                                             <option value="10-October">10-October</option>
			                                             <option value="11-November">11-November</option>
			                                             <option value="12-December">12-December</option>
			                                             </select>
							   <span style="padding-left:15px;"><select name="year"></span>
							                             <option value="default">Year</option>
                                                         <option value="2015">2015</option>
                                                         <option value="2016">2016</option>
                                                         <option value="2017">2017</option>
                                                         <option value="2018">2018</option>
			                                             <option value="2019">2019</option>
			                                             <option value="2020">2020</option>
			                                             <option value="2021">2021</option>
			                                             <option value="2022">2022</option>
										                 <option value="2023">2023</option>
			                                             <option value="2024">2024</option>
			                                             <option value="2025">2025</option>
			                                            </select>
							</p>
						   </div>
						   <div class="t">
						       <label>CCV/CVV:</label>
								<p style="margin-left:130px;"><input name="ccv" type="text" value="" size="3"  maxlength="16" onkeypress="return isNumber(event)" />
								 <a id="open-event" href="https://www.cvvnumber.com/cvv.html" target="_blank"  style="font-size:11px">What's this?</a>
								</p>
						   </div>	
						   	<div style="float:left;" id="yy">
							 <input name="checkout" id="finish_checkout" type="button" value="go back" href="javascript:history.go(-1)"/>
							 </div>
                            <div id="button">
						
							   <div id="sub">
							 
                              <input name="pay" id="" type="submit" value="PLACE YOUR ORDER" />
							  </div>
							</div>
                        </div> 
                   </form>
			</div>
	   </div>
   </div>
   <?php include_once("footer.php");?>
</div>
   
</body>
</html>
<?php
if(isset($_POST["pay"])){
 
        $sid = $_SESSION["sess_id"];
        $dt = date("Y-m-d");
		$ostt="Pending";
		$type = $_POST["tp"];
		$name = $_POST["cardname"];
		$num=$_POST["cardnumber"];
		$cv=$_POST["ccv"];
		$month=$_POST["month"];
		$year=$_POST["year"];
		$my=$month.$year;
	    $result2=mysqli_query($conn,"select * FROM card where Card_Number='$num'");
		$row2=mysqli_fetch_assoc($result2);
		$bankid=$row2["Card_ID"];

		if($row2["Card_Type"]==$type && $row2["Card_Name"]==$name && $row2["Card_Number"]==$num && $row2["Card_Code"]==$cv && $row2["Card_Expirydate"]==$my)
		{		    
	      $re= mysqli_query($conn, " insert into cus_order (Order_Date, Order_Status, Ship_Name, Ship_Contactnumber, Ship_Address, Ship_City, Ship_Postcode, Ship_State, Member_ID)values
												('$dt','$ostt','$un','$cn','$ad','$cyy','$pcc','$stt','$sid')");
		  if($re)
          {
			  $resultorder = mysqli_query($conn, "select * from cus_order where Order_ID = (select MAX(Order_ID) from cus_order)");
		if($roworder = mysqli_fetch_assoc($resultorder))
	    {
			$orderid=$roworder['Order_ID'];
			if(isset($_SESSION["cart_array"])){
				$cartTotal="";
				$i = 0; 
			  foreach ($_SESSION["cart_array"] as $each_item){ 
			       $item_id = $each_item['item_id'];
				   $proqty=$each_item["quantity"];
				   	$qurey = "SELECT * FROM product INNER JOIN images on product.Images_ID=images.Images_ID where Product_ID ='$item_id' LIMIT 1";
		             $sql = mysqli_query($conn, $qurey);
		          while ($row = mysqli_fetch_array($sql)) {
			         $product_name = $row["Product_Name"];
			         $price = $row["Product_Price"];
			         $pi = $row["Images_1"];
				
		         }
		          $pricetotal = $price * $each_item['quantity'];
                   $cartTotal = $pricetotal + $cartTotal ;
				mysqli_query($conn,"INSERT INTO `orderdetail`( `Order_ID`, `Product_ID`, `Order_Quantity`, `Order_Price`) 
					VALUES ('$orderid','$item_id','$proqty','$cartTotal')");
				 mysqli_query($conn, "update product set Product_Quantity = Product_Quantity-$proqty where Product_ID= $item_id");
				 //header("location:login.php");
				
			  }
			}
			

			?>
					<script type='text/javascript'>
						alert("Your order successful");
					window.location.href = 'Vieworder.php'; 
					</script>
					
			<?php
			 
			   unset($_SESSION["cart_array"]);
			    //header("location:vieworder.php"); 
				}
			else
			{
			?>
					<script type='text/javascript'>
						alert("Not valid input");
					</script>
			<?php	
			}
			// echo "Success"; 
		  }
          else
          {
            echo "Error";
		  }	  
	 
         }
		else
		{
			$message = "wrong answer";
           echo "<script type='text/javascript'>alert('$message');</script>";
			
		}
	}	
?>



