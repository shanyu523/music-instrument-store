<?php    
include("connection.php");    
session_start();
$ssid=$_SESSION['sess_id'];
if(isset($_SESSION['sess_id'])){
$sid=$_SESSION['sess_id'];
}else{
	header("location:login.php");
}
?>


<!doctype html>
<html>
<head>
<title>Homepage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/nav.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/script.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>

<style type="text/css">
	#content
		{
		 
		   width:100%;
		   padding-top:20px;
		  border-top:1px solid #d3d3d3;
		  float:left;
			
		}
		#detail th {
			width:300px;
			padding:15px;
			text-align:left
		}
		
		#detail td {
			width:600px;
			padding:15px;
			text-align:left;
		}
		#sidebar
		{

	      height:800px;
		   width:250px;
		   background-color:#ffffff;
		   margin-left:5px;
		   border:1px solid #d3d3d3;
		   float:left;
		}
	
		#menu li.mp
		{
			border-radius:5px;
			margin-bottom:10px;
	background-color:#dbdbdb;
			text-align:center;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.ep
		{
			border-radius:5px;
		background-color:#dbdbdb;
			text-align:center;
			margin-bottom:10px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
	    #menu li.mo
		{
			border-radius:5px;
			text-align:center;
		background-color:#dbdbdb;
			margin-bottom:10px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.lg
		{
			border-radius:5px;
			text-align:center;
			background-color:#dbdbdb;
		    padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu ol
		{
		
		   margin-top:30px;
		  list-style:none;
		  letter-spacing:2px;
		
		}
		#menu a
		{
		  display:inline-block;
		  padding:6px;
		 text-decoration: none;
		}
.od
{
  margin:25px;
    padding-left:25px;
  float:left;
}
table{

	border:1px solid black;
    
}

th
{
		padding:25px;
	background-color:#d3d3d3;
	
}
td{
    padding:15px;
}
.h
		{
		
			text-align:center;
			font-size:16pt;
		    padding:5px;
			margin-bottom:20px;
		}

</style>

</head>
<body>
    <div id="wrapper">

	    <?php include_once("header1.php");?>
	<div id="content">
      <div id="sidebar">
		       <div id="menu">
			            <div class="h">
				<?php $result=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
				$row2=mysqli_fetch_array($result);?>
		                <b><?php echo $row2['Agent_UserName'];?></b>
				
					 </div>
	


			     <ol>
                 <li class="mp"><a href="member_profile.php">Member profile</a></li>
			    <li class="ep"><a href="member_edit.php">Edit Profile</a></li>
			    <li class="mo"><a href="changepassword.php">Change Password</a></li>
				<li class="mo"><a href="vieworder.php">My Orders</a></li>
				<li class="mo"><a href="orderhistory.php">Order History</a></li>
			    <li class="lg"><a href="logout.php">Logout</a></li>
			   </ol>
			  </div>
	   </div> 
		   <div class="od">
	          <div class="ho">
		       <h2>Orders History	</h2>
		      </div>
	
	     <table style="boder:none;">
	     <tr>
                <th>Order ID</th>
                <th>Order Date</th>
                <th>Order Status</th>
                <th>Total Cost</th>
         </tr>
           <?php
		$result=mysqli_query($conn,"SELECT * FROM cus_order WHERE Order_Status='Arrived' AND Member_ID=$ssid");
		$total=0;
		$num_rec_per_page=10;
		if (isset($_GET["page"])) { 
			$page  = $_GET["page"]; 
		} else { 
			$page=1; 
		}; 
		$start_from = ($page-1) * $num_rec_per_page; 
		$count=0;
		$rowcount = mysqli_num_rows($result);
		while($row=mysqli_fetch_array($result))
		{
			$oid=$row['Order_ID'];
			$result2=mysqli_query($conn, "SELECT * FROM orderdetail WHERE Order_ID=$oid");
			$row2=mysqli_fetch_array($result2);
			{
				$total+=$row2['Order_Price'];
			}
		   ?><tr>
						<td><?php echo $row['Order_ID'] ?></td>
						<td><?php echo $row['Order_Date']?></td>
						<td><?php echo $row['Order_Status'] ?></td>
						<td><?php echo $total ?></td>
				 </tr>
			
			   <?php
			$count++;
		}?>
	   </table>
<?php
$rowcount = mysqli_num_rows($result);
$total_records = mysqli_num_rows($result);  //count number of records
$total_pages = ceil($rowcount / $num_rec_per_page); 

echo "<a href='orderhistory.php?page=1'>".''."</a> "; // Goto 1st page  

for ($i=1; $i<=$total_pages; $i++) { 
            echo "<div class='pg'><a href='orderhistory.php?page=".$i."'>".$i."</a> </div>"; 
}; 
echo "<a href='orderhistory.php?page=$total_pages'>".''."</a> "; // Goto last page
?>	

	</div>
  </div>
</div>
	 <?php include_once("footer.php");?>   

</body>
</html>