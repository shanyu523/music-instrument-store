<?php 
 // Start session first thing in script
// Connect to the MySQL database  
session_start();
include ("connection.php"); 
//   Step 1 (if user attempts to add something to the cart from the product page)

if (isset($_GET['pd'])) {
    $pid = $_GET['pd'];
	$wasFound = false;
	$i = 0;
	// If the cart session variable is not set or cart array is empty
	if (!isset($_SESSION["cart_array"]) || count($_SESSION["cart_array"]) < 1) { 
	    // RUN IF THE CART IS EMPTY OR NOT SET
		$_SESSION["cart_array"] = array(0 => array("item_id" => $pid, "quantity" => 1));
	} else {
		// RUN IF THE CART HAS AT LEAST ONE ITEM IN IT
		foreach ($_SESSION["cart_array"] as $each_item) { 
		      $i++;
		      while (list($key, $value) = each($each_item)) {
				  if ($key == "item_id" && $value == $pid) {
					  // That item is in cart already so let's adjust its quantity using array_splice()
					  array_splice($_SESSION["cart_array"], $i-1, 1, array(array("item_id" => $pid, "quantity" => $each_item['quantity'] + 1)));
					  $wasFound = true;
				  } // close if condition
		      } // close while loop
	       } // close foreach loop
		   if ($wasFound == false) {
			   array_push($_SESSION["cart_array"], array("item_id" => $pid, "quantity" => 1));
		   }
	}
	header("location: shoppingcart.php"); 
    exit();
}
?>
<?php 

//       Section 2 (if user chooses to empty their shopping cart)

if (isset($_GET['cmd']) && $_GET['cmd'] == "emptycart") {
    unset($_SESSION["cart_array"]);
	  $_SESSION['msgs'] = array('Your cart has been emptied.');
}
?>
<?php 
      //Section 3 (if user chooses to adjust item quantity)
if (isset($_POST['item_to_adjust']) && $_POST['item_to_adjust'] != "") {
    // execute some code
	$item_to_adjust = $_POST['item_to_adjust'];
	$quantity = $_POST['quantity'];
	$quantity = preg_replace('#[^0-9]#i', '', $quantity); // filter everything but numbers
	if ($quantity >=99) { $quantity =99; }
	if ($quantity < 1) { $quantity = 1; }
	if ($quantity == "") { $quantity = 1; }
	if(isset($_SESSION["cart_array"])) {
	$i = 0;
	foreach ($_SESSION["cart_array"] as $each_item) { 
		      $i++;
		      while (list($key, $value) = each($each_item)) {
				  if ($key == "item_id" && $value == $item_to_adjust) {
					  // That item is in cart already so let's adjust its quantity using array_splice()
					 array_splice($_SESSION["cart_array"], $i-1, 1, array(array("item_id" => $item_to_adjust, "quantity" => $quantity)));
				  } // close if condition
		      } // close while loop
	}	 // close foreach loop
 }
}
?>
<?php 
// Section 4 (if user wants to remove an item from cart)
if (isset($_POST['index_to_remove']) && $_POST['index_to_remove'] != "") {
    // Access the array and run code to remove that array index

	$key_to_remove = $_POST['index_to_remove'];
	if (count($_SESSION["cart_array"])<=0) {
		unset($_SESSION["cart_array"]);
	} else {
		unset($_SESSION["cart_array"]["$key_to_remove"]);
		sort($_SESSION["cart_array"]);
          $_SESSION['msgs'] = array('Item removed from your cart.');     
	}
}
?>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Your Shopping Cart</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/mainn.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/navv.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/script.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<script src="js/jquery.min.js"></script>
<style type="text/css">
.h3{margin-left:145px;width:930px;
    border-bottom: none;
 text-transform:capitalize;
    font-size:30px;
	color: #202020;
margin-bottom: 6px;
line-height: 1.5;
    padding-bottom: 10px;
    font-family: "PT Sans", "Helvetica Neue", Verdana, Arial, sans-serif;}
#cart{ margin:5px; }

.table{ margin-left:145px; width:950px;  height:auto;}

.alert{color:red;}
.c input[type="submit"]
{
	min-width:150px;
	    font-size: 15px;
		background: #4e0055;
		    cursor: pointer;
    color: #FFFFFF;
	    padding: 10px 15px;
		font-family: "Arvo", "Helvetica Neue", Verdana, Arial, sans-serif;
}
.title{
	display: table;
    width: 100%; 
	
	    border-bottom: 1px solid #E1E1E1;
    font-family: "Arvo", "Helvetica Neue", Verdana, Arial, sans-serif;
    border-bottom: 1px solid #E1E1E1;
    padding: 10px 0;
}
.cart,.t,.q
{
	display: table-cell;
	    white-space: nowrap;
		    font-size: 14px;
    line-height: 1.5;
    text-align: center;
}

</style>
 <?php include_once("header1.php");?>
<div id="cart">

<div class="h3">
<h3>Shopping Cart</h3>
</div>

<?php
 if( isset($_SESSION['msgs']) && is_array($_SESSION['msgs']) && count($_SESSION['msgs']) >0 ) {
              ?>
              <ul>
               <?php 
                  foreach ($_SESSION['msgs'] as $msgs) {
                   echo '<li style="list-style:none; margin-left:110px; color:red;"><strong>'.$msgs.'</strong></li>';
                  }
                  ?>
              </ul>
        
            <?php
            unset($_SESSION['msgs']);
          }
        ?>
</div>
 <div class="table">
<?php if (isset($_SESSION["cart_array"]) && count($_SESSION["cart_array"])>0) {?>
     
	     <div class="title" style="width:100%">
		 
		   <div class="cart" rowspan="1" style="width:16.66667%;">
		   <span class="nobr">Products</span>
		   	
		   </div>
	        <div class="t" style="width:32.29167%;"> </div>
		   <div class="t" colspan="1" style="width:13.54167%;">
		      <span class="nobr" >Unit Price</span>
		     </div>
	      <div rowspan="1" class="q" style="width:18.75%;">Quantity</div>
		  <div class="t" colspan="1" style="padding-right:20px; width:16.66667%;">Total</div>
		   <div class="t" style="width:25%;"> </div>
		</div>
	
		  <?php
		 $i = 0; 
		 $cartTotal = "";
		 $product_id_array = '';
	     $cart_count = count($_SESSION['cart_array']);
        foreach ($_SESSION["cart_array"] as $each_item) { 
		   $item_id = $each_item['item_id'];
		   $cart_count=count($_SESSION["cart_array"]);
		   $qurey = "SELECT * FROM product INNER JOIN image on product.Images_ID=image.Images_ID where Product_ID	 ='$item_id' LIMIT 1";
		$sql = mysqli_query($conn, $qurey);
		while ($row = mysqli_fetch_array($sql)) {
			$product_name = $row["Product_Name"];
			$price = $row["Product_Point"];
			$pi = $row["Images_1"];
				
		}
		   $pricetotal = $price * $each_item['quantity'];
           $cartTotal = $pricetotal + $cartTotal ;
		 $product_id_array .= "$item_id-".$each_item['quantity'].","; 
		 ?>
		  <div class="cartbody" style="box-sizing:border-box; ">
		     <div class="item" style="box-sizing:border-box; width:100%;">
			    <div class="productimage" style=" display:table-cell; width:135px;">
				 <img src="images\<?php echo $pi ?>" width="120px" height="120px" />
				</div>
				<div class="productname" style="display:table-cell; width:348px; vertical-align:top;" >
				   <div class="nn">
				    <?php echo '<a href="product.php?id=' . $item_id . '">' . $product_name . '</a>'?>
				   </div>
			   </div>
			   
	            <div class="productprice" style="display:table-cell; width:110px; text-align:center;">
				  <span style="color: #66006e;"><?php echo 'RM' .number_format($price, 2, '.', ',')  . ''?></span>
				</div>
						<script type="text/javascript">
									
											
												$("#update1").click(function()
												{
												var quantity=$("#quantity").val();
												
												if(isNaN(quantity) || !quantity.match(/^\+?[0-9(),.-]+$/))
												{
													$("#errormsg").text("Valid number only").prepend(\'<img src="wrong.png" />\');
													return false;
													
												}
												else
												{
											
													return true;
													
												}
										});
									
								</script>
								<?php 
								$productquery=mysqli_query($conn,"SELECT * FROM product where Product_ID='$item_id'");
								$productfetch=mysqli_fetch_assoc($productquery);
								$productfetch["Product_Quantity"];
								?>
				<div class="productqty" style="display:table-cell; width:190px;text-align:center;" >
				  <form action="shoppingcart.php" method="post">
		         <input name="quantity" id="quantity" type="number" max="<?php echo $productfetch["Product_Quantity"];?>" value="<?php if($each_item['quantity']>$productfetch["Product_Quantity"]){
					                                                             echo $productfetch["Product_Quantity"];}				
																				else{								
																		        echo $each_item['quantity'];} ?>" style="width:4em" min="1" />
		         <input name="adjustBtn <?php echo $item_id;?>" id="update1" type="submit" value="update" name="item" />
		        <input name="item_to_adjust" type="hidden" value="<?php echo $item_id ?>" /></form>
				</div>
				<div class="total" style="width:133px;  text-align:center; display:table-cell;">
				  <div class="">
				    <span style="font-weight:bold; font-weight:600"><?php echo' RM' . number_format($pricetotal, 2, '.', ',') . ''?></span>
				  </div>
				</div>
				<div class="tu" style="width:80px; ">
				 <?php echo'<form action="shoppingcart.php" method="post" name="delete" ><input name="deleteBtn' . $item_id . '" type="submit" value="delete"  /><input name="index_to_remove" type="hidden" value="' . $i . '" /></form>'?>
				</div>
				
		 <?php
		}
			?>

<div style="float:left;">		
<div style="text-align:left;font-weight:600; padding-top:50px;">

<a href="shoppingcart.php?cmd=emptycart"><b>Clear Your Cart</b></a>

</div>
<div style="padding-top:25px;">
<a href="javascript:history.go(-1)"><b>Continue Shopping</b></a>
</div>
</div>

			<div class="total"style="margin-right:55px; float:right; margin-top:15px; ">
            <span style="font-weight:bold; font-size:14pt;">Total:</span><span style="font-size:14pt"><?php echo ' RM'.number_format($cartTotal, 2, '.', ',') .' '?></span>
			<div class="c" style="clear:both; padding-top:35px;">
              <form action="checkout.php" method="post" name=""/>
             <input name="checkout" id="goToCheckout" type="submit" value="CHECKOUT"  />
             </form>
           </div>
      
	       </div>
		  
			 </div>
		
  		
			 </div>
			
   </div>
      


    
 <?php include_once("footer.php");?>



<?php
}else{
	echo'<div class="alert"><strong>Please, add something to your cart.</strong></div>';
	 echo'<a href="javascript:history.go(-2)"><b>Continue Shopping</b></a>';
}
?>


</body>
</html>