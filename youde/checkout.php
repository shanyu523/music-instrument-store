<?php
error_reporting(0);
ini_set('display_errors', 0);
session_start();
include("connection.php");
if(isset($_SESSION['sess_id'])){
$sid=$_SESSION['sess_id'];
}else{
  
	header("location:access_denied.php");
}
$result=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
$row=mysqli_fetch_array($result);
if (isset($_POST["continue"]))
{
	$_SESSION["uname"]= $_POST["name"];
	$_SESSION["cnum"] = $_POST["contactnumber"];
	$_SESSION["add"]=$_POST["address"];
	$_SESSION["cy"]=$_POST["city"];
    $_SESSION["pc"]=$_POST["postcode"];
	$_SESSION["st"]=$_POST["state"];
    header('Location:payment.php');
}
?>


<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/nav.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.js"></script>
<script src="js/additional-methods.min.js"></script>
<style type="text/css">
#check
{
  margin:15px;
  height:1000px;
}
#shipping
{
	width:700px;
	height:500px;
    margin-left:10px;
	border:1px solid #dde2e4;
	float:left;
}
#h1
{
	width:700px;
	height:50px;
	font-size:18pt;
    font-weight:700;
	padding:2px;
	border-bottom:1px solid #dde2e4;;
}
#shp
{
	width:650px;
	height:300px;
    margin:auto;
}
label
{
font-family: Georgia, "Times New Roman", Times, serif;
  text-align: right;
    clear: both;
    float:left;
    margin-right:10px;
}
.text
{
   margin:20px;
}
input[type="text"]
{
   border:1px solid #d1d1d1;
   border-radius:4px;
   height:32px;
}
#add input[type="text"]
{
	height:50px;
}
select
{
  height:30px;
  border:1px solid #d1d1d1;
   border-radius:4px;
}
option {
    font-weight: normal;
    display: block;
    padding: 0px 2px 1px;
    white-space: pre;
    min-height: 1.2em;
}
#order
{
	width:550px;
	height:500px;
	margin-right:10px;
	border:1px solid #dde2e4;
	float:right;
}
#h2
{
	border-bottom:1px solid #dde2e4;
	height:50px;
	width:350px;
	font-size:18pt;
    font-weight:700;
	padding:2px;
}

#button
{
   float:right;
margin-right:80px;
margin-top:50px;

}
.sub
{
	float:left;
  margin-top:50px;
}
.sub input[type="submit"]
{
 border-radius:5px;
 border:1px solid #f36b2d;
 background-color:#f36b2d;
 text-align: center;
 color:white;
 font-weight:700;
 height:40px;
}
#button input[type="submit"]
{
 border-radius:5px;
 border:1px solid #f36b2d;
 background-color:#f36b2d;
 text-align: center;
 color:white;
 font-weight:700;
 height:40px;
}
label.error {
    float: none; 
  color:red;
    display: inline-block;
  padding-left:25px;
  vertical-align: middle;
 }

label.passValid
{
	color:green;
	 float: none; 
    display: inline-block;
  padding-left:25px;
  vertical-align:middle;

}

</style>

<script>
function goBack() {
    window.history.back();
}
</script>
</head>
<body>

    <?php include_once("header.php");?>
	   <div id="check"> 
                 <div id="shipping">
			
				    <form name="shipping" id="ship" action="" method="post"/>
				     <div id="h1">
					    <p>Your Shipping Details</p>
					<?php $result2=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
										$row2=mysqli_fetch_array($result2);?>	
					 </div>
				        <div id="shp">
						  <div class="text">
						    <label>Name:</label>
						    <p style="margin-left:130px;"><input type="text" name="name" size="30px" value="<?php echo $row2["Member_Name"];?>"/></p>
						 </div>
						  <div class="text">
		                    <label>Mobile Number:</label>
							<p><input type="text" name="contactnumber"  size="30px" value="<?php echo $row2["Member_Contactnumber"];?>"/></p>
						  </div>
						   <div class="text">
						   <label>Address:</label>
						    <p><div id="add" style="margin-left:126px;" ><input type="text" name="address" size="30px" class="add" value="<?php echo $row2["Member_Address"];?>"/></div></p>
						   </div>
						   <div class="text">
						    <label>City:</label>
						    <p style="margin-left:125px;"><input type="text" name="city"  size="30px" value="<?php echo $row2["Member_City"];?>"/></p>
							</div>
						   <div class="text">
						   <label>Postcode:</label>
						   <p style="margin-left:126px;"><input type="text" name="postcode" size="30px" value="<?php echo $row2["Member_Postcode"];?>"/></p>
						   </div>
						    <div class="text">
					         <label class="">State</label>
                             <p style="margin-left:124px;"> <select name="state" /></p>
                             <option value="<?php echo $row['Member_State']; ?>"><?php echo $row2['Member_State']; ?></option>
                                             <option value="Johor">Johor</option>
			<option value="Kedah">Kedah</option>
			<option value="Kelantan	">Kelantan	</option>
			<option value="Malacca">Malacca</option>
			<option value="Negeri Sembilan">Negeri Sembilan</option>
			<option value="Pahang">Pahang</option>
			<option value="Perak">Perak</option>
			<option value="Perlis">Perlis</option>
			<option value="Penang">Penang</option>
			<option value="Selangor">Selangor</option>
			<option value="Terengganu">Terengganu</option>
	         <option value="Sabah">Sabah</option>
			<option value="Sarawak">Sarawak</option>
			<option value="Kuala Lumpur">WP Kuala Lumpur</option>
			<option value="Labuan">WP Labuan</option>
            <option value="Putrajaya">WP Putrajaya</option>
			</select>
						 </div>
		
       
						    
						</div>
						 <div id="button">
					
			              <input type="submit" name="continue" value="continue" href=""/>
						
					     </div>
                          <div class="sub">	
						  <input name="checkout" id="finish_checkout" type="submit" value="CANCEL" onclick="goBack()"/>
					    </div>
				
				 </form>
				 </div>
				
				        <div id="order">
						<h2>View Order</h2>
				
                            <?php
		 $i = 0; 
		 $cartTotal = "";
		 $product_id_array = '';
	     $cart_count = count($_SESSION['cart_array']);
        foreach ($_SESSION["cart_array"] as $each_item) { 
		   $item_id = $each_item['item_id'];
		   $cart_count=count($_SESSION["cart_array"]);
		   $qurey = "SELECT * FROM product INNER JOIN image on product.Images_ID=images.Images_ID where Product_ID	 ='$item_id' LIMIT 1";
		$sql = mysqli_query($conn, $qurey);
		while ($row = mysqli_fetch_array($sql)) {
			$product_name = $row["Product_Name"];
			$price = $row["Product_Point"];
			$pi = $row["Images_1"];
				
		}
		   $pricetotal = $price * $each_item['quantity'];
           $cartTotal = $pricetotal + $cartTotal ;
		 $product_id_array .= "$item_id-".$each_item['quantity'].","; 
		 ?>
           <div class="cartbody" style="box-sizing:border-box; ">
		     <div class="item" style="box-sizing:border-box; width:100%; float:left;">
			   <span style="">Product_Image</span>	
			    <div class="productimage" style=" display:table-cell; width:45px;">
			
				 <img src="images\<?php echo $pi ?>" width="60px" height="120px" />
				</div>
				 <span>price</span>	
				<div class="productname" style="display:table-cell; width:190px; float:left;" >
				   <div class="nn">
				    <?php echo $product_name ?>
				   </div>
			   </div>
		
				<div class="total" style="width:90px;  text-align:center; display:table-cell;">
				  <div class="">
				    <span style="font-weight:bold; font-weight:600"><?php echo' RM' . number_format($pricetotal, 2, '.', ',') . ''?></span>
				  </div>
				</div>
			
				
		 <?php
		}
			?>

		</div>
	</div>
						

					

</div>
</div>
   <?php include_once("footer.php");?>
</body>
</html>
<script>
      $('document').ready(function()
	  { 
	
	      $("#ship").validate({
              
			 errorPlacement: function (error, element) {
            error.insertAfter(element);
            if (element.hasClass('text')) {
                element.next().removeClass('passValid').addClass('passError');
            }
        },
		success: function (label) {
            if (label.prev().hasClass('text')) {
                label.text("valid~");
            }
        },
		 highlight: function (element, errorClass, validClass) {
            if ($(element).hasClass('text')) {
                $(element).next().removeClass('passValid').addClass('passError');
            } else {
                $(element).addClass(errorClass).removeClass(validClass);
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            if ($(element).hasClass('text')) {
                $(element).next().removeClass('passError').addClass('passValid');
            } else {
                $(element).removeClass(errorClass).addClass(validClass);
            }
        },
			    rules: {
                    name:{
					  required:true,
				      maxlength:30,
					  minlength:5,
					  noSpace:true,
					  user:true
			          },
            
                    email: {
                        required: true,
                        email: true,
						noSpace:true },
                    password: {
                        required: true,
                        minlength:6,
						maxlength:12,
						noSpace:true
                    },
					confirmpassword: {
			               required:true,
                           equalTo: "#password",
						   noSpace:true
                    },
					contactnumber:{
					     required:true
						 
					},
		           birthdate:{ 
					  required:true,
					  date:true
		           },
					address:{
						required:true						
	                },
					city:{
						required:true,
						lettersonly:true,
						noSpace:true
					},
					postcode:{
						required:true,
						number:true,
						maxlength:5,
						minlength:5,
						noSpace:true
					},
					
					agree:{
						required:true
					}
                },
                messages: {
                    name: { 
					    required:"Please Enter your name",
					   minlength:"Customer name must have at least {0} characters",
					
				     },
	                password: {
                        required: "Please Enter your password",
                        minlength: "Your password must be at least {0} characters long"
                    },
					confirmpassword:{
						    required:"Please Re-type your password",
						    equalTo: "password no match"
					},
                    email: {
						 required:"Please Enter your email address",
						 email:"Please Enter a valid email address",
						 remote:"The Email Already exists"
						},
				    contactnumber: {
						 required:"Please Enter your contact number"
					
						},
				
				   address:{
					   required:"Please Enter your address"
				     },
				   city:{
					  required:"Please Enter your city",
					  lettersonly:"Please Enter valid city"
				  },
				  postcode:{
					  required:"Please Enter your postcode",
					  number:"Please Enter valid postcode",
					  minlength:"Please Enter valid postcode"
				  },

				 
                },
                submitHandler:function(form) {
                    form.submit();
					$('#ship').each(function(){
               
					});
                }
			
            }); 
      jQuery.validator.addMethod("user", function(value, element) { 
		return this.optional(element) || /^[a-z0-9\.\-_]{3,30}$/i.test(value); 
		}, "Please choise a username with only a-z 0-9.");
     jQuery.validator.addMethod("num", function(value, element) {
    return this.optional(element) || /^6?01\d{8}$/.test(value);
     }, ""); 
    
   jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
  return arg != value;
 }, "Value must not equal arg.");

     jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "Space are not allowed");
 
});
  
</script>