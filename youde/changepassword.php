<?php    
include("connection.php");    
session_start();
if(isset($_SESSION['sess_id'])){
$sid=$_SESSION['sess_id'];
}else{
	header("location:Homepage.php");
}
?>

<!DOCTYPE HTML>
<html>
<head><title>Member_Edit</title>
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/nav.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.js"></script>
<script src="js/additional-methods.min.js"></script>
<style type="text/css">
	#btnRegister input[type="submit"]{
			color:white;
			width:100px;
			font-size:10pt;
			background-color:orange;
			border:1px solid rgb(234,120,0);
			border-radius:5px;
			font-weight:bold;
			font-family:Nirmala UI;
			opacity:0.75;
		}		
		
		#btnEdit input[type="submit"] {
			color:white;
			width:80px;
			font-size:10pt;
			background-color:orange;
			border:1px solid rgb(234,120,0);
			border-radius:5px;
			font-weight:bold;
			font-family:Nirmala UI;
			opacity:0.75;
		}		
		
		#btnEdit singkeongyainput[type="submit"]:hover {
			opacity:1;
		}
		
		legend {
		    font-size:14px;
			 color: #4c4c4c;
			font-weight:bold;
			 background: #d9d9d9;
			padding:5px;
		}
		#content
		{
		 
		   width:100%;
		   padding-top:20px;
		  border-top:1px solid #d3d3d3;
		  float:left;
			
		}
		#detail th {
			width:300px;
			padding:15px;
			text-align:left
		}
		
		#detail td {
			width:600px;
			padding:15px;
			text-align:left;
		}
		#sidebar
		{

	      height:800px;
		   width:250px;
		   background-color:#ffffff;
		   margin-left:5px;
		   border:1px solid #d3d3d3;
		   float:left;
		}
	
		#menu li.mp
		{
			border-radius:5px;
			margin-bottom:10px;
			background-color:#dbdbdb;
			text-align:center;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.ep
		{
			border-radius:5px;
			background-color:#dbdbdb;
			text-align:center;
			margin-bottom:10px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
	    #menu li.mo
		{
			border-radius:5px;
			text-align:center;
	        	background-color:#dbdbdb;
			margin-bottom:10px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.lg
		{
			border-radius:5px;
			text-align:center;
				background-color:#dbdbdb;
		    padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu ol
		{
		
		   margin-top:30px;
		  list-style:none;
		  letter-spacing:2px;
		
		}
		#menu a
		{
		  display:inline-block;
		  padding:6px;
		 text-decoration: none;
		}
		.form
		{
          padding-left:20px;
		  margin:20px;
		 float:left;
		}
		#pic
		{
		   width:200px;
		   height:200px;
		   margin-left:20px;
		}
		.h
		{
		
			text-align:center;
			font-size:16pt;
		    padding:5px;
			margin-bottom:20px;
		}
		#h2
		{
			padding:15px;
				text-align:center;
			font-size:16pt;
			margin-bottom:20px;
		}
		label
		{
			  font-family: Georgia,"Times New Roman", Times, serif;
  text-align:center;
  margin-top:6px;
    clear: both;
    float:left;
	 margin-right:10px;
		}
		input[type="password"]
{
   border:2px solid #d1d1d1;
   outline-color: #E45D5D;
   border-radius:2px;
    font-size: 15px;
   height: 40px;
    padding: 0 8px;
}
input[type="submit"]
{
	background:orange;
	border:1px solid orange;
	border-radius:4px;
   font-weight: 700;
	 cursor: pointer;
	color:white;
	padding:8px;
	
}
label.error {
    float: none; 
  color:red;
    display: inline-block;
  padding-left:25px;
  vertical-align: middle;
 }
	
	</style>


</head>
<body>
    <?php include_once("header1.php");?>
<div class="content">
    <div id="sidebar">
     <div class="side">
		       <div id="menu">
	
                       <div class="h">
				<?php $result=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
				$row2=mysqli_fetch_array($result);?>
		                <b><?php echo $row2['Member_Name'];?></b>
				
					 </div>
	
			    <ol>
	            <li class="mp"><a href="member_profile.php">Member profile</a></li>
			    <li class="ep"><a href="member_edit.php">Edit Profile</a></li>
			    <li class="mo"><a href="changepassord.php">Change Password</a></li>
				<li class="mo"><a href="vieworder.php">My Orders</a></li>
				<li class="mo"><a href="orderhistory.php">Order History</a></li>
			    <li class="lg"><a href="logout.php">Logout</a></li>
			   </ol>
			  </div>
		 </div>
	</div>
	    <div class="form"> 
		     <div id="h2">
             <p>Change Password</p>
		     </div>
	
                     <?php
						     if(isset( $_POST['con']))
							 {
						     	$password = $_POST['password'];
	                    
							$qry = "UPDATE member SET Member_Password='".md5($_POST['password'])."' WHERE Member_ID=$sid";
							$result = mysqli_query($conn,$qry);
							if($result){
							  	  ?>
								<script type='text/javascript'>alert('Your password changed,"success"');</script>";
								<?php
	                 
							 }
							 else{
								  ?>
								<script type='text/javascript'>alert('error');</script>";
								<?php
								
							 }
						}
						  ?>
						<div style="margin-top:20px; margin-left:30px; width:700px;">
						 <form name="" action="" method="post" id="pass"/>
						    <div class="ed">
						         <label>Password</label>
							  <p style=" margin-left:80px"> <input type="password" name="password" size="25" /></p>
							</div>
							<div class="ed">
							     <label>Confirm <br/>Password</label>
							   <p style="padding-top:8px"><input type="password" name="cpassword" size="25"/></p>
							 </div>
							 <div style="float:left; margin-top:15px; margin-left:100px;">
							  <input type="submit" name="con" value="CONFIRM"/>
						  </div>
                        </form>
                   </div>
			</div>	
</body>		
</html>
<script>
      $('document').ready(function()
	  { 
	
	      $("#pass").validate({
              
			 errorPlacement: function (error, element) {
            error.insertAfter(element);
            if (element.hasClass('text')) {
                element.next().removeClass('passValid').addClass('passError');
            }
        },
		success: function (label) {
            if (label.prev().hasClass('text')) {
                label.text("valid~");
            }
        },
		 highlight: function (element, errorClass, validClass) {
            if ($(element).hasClass('text')) {
                $(element).next().removeClass('passValid').addClass('passError');
            } else {
                $(element).addClass(errorClass).removeClass(validClass);
            }
        },
        unhighlight: function (element, errorClass, validClass) {
            if ($(element).hasClass('text')) {
                $(element).next().removeClass('passError').addClass('passValid');
            } else {
                $(element).removeClass(errorClass).addClass(validClass);
            }
        },
			    rules: {
                    name:{
					  required:true,
				      maxlength:30,
					  minlength:5,
					  noSpace:true,
					  user:true
			          },
            
                    email: {
                        required: true,
                        email: true,
						noSpace:true },
                    password: {
                        required: true,
                        minlength:6,
						maxlength:12,
						noSpace:true
                    },
					cpassword: {
			               required:true,
                           equalTo: "#password",
						   noSpace:true
                    },
					contactnumber:{
					     required:true,
						 number:true,
						 num:true,
						noSpace:true
					},
		           birthdate:{ 
					  required:true,
					  date:true
		           },
					address:{
						required:true						
	                },
					city:{
						required:true,
						lettersonly:true,
						noSpace:true
					},
					postcode:{
						required:true,
						number:true,
						maxlength:5,
						minlength:5,
						noSpace:true
					},
					state:{
						required:true,
                      valueNotEquals: "default"
					},
					agree:{
						required:true
					}
                },
                messages: {
                    name: { 
					    required:"Please Enter your name",
					   minlength:"Customer name must have at least {0} characters",
					
				     },
	                password: {
                        required: "Please Enter your password",
                        minlength: "Your password must be at least {0} characters long"
                    },
					cpassword:{
						    required:"Please Re-type your password",
						    equalTo: "password no match"
					},
                    email: {
						 required:"Please Enter your email address",
						 email:"Please Enter a valid email address",
						 remote:"The Email Already exists"
						},
				    contactnumber: {
						 required:"Please Enter your contact number",
						 number:"Please enter a valid contact number",
						 num:"invalid contact number"
						},
				
				   address:{
					   required:"Please Enter your address"
				     },
				   city:{
					  required:"Please Enter your city",
					  lettersonly:"Please Enter valid city"
				  },
				  postcode:{
					  required:"Please Enter your postcode",
					  number:"Please Enter valid postcode",
					  minlength:"Please Enter valid postcode"
				  },
				state:{
					  required:"Please choose your state",
					  valueNotEquals: "Please select state!"
					 
				},
				agree:{
					required:"required"
				},
				
				 
                },
                submitHandler:function(form) {
                    form.submit();
					$('#pass').each(function(){
               
					});
                }
			
            }); 
      
 
});
  
</script>
