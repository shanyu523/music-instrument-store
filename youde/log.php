<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Youde</title>

  <!-- Bootstrap CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="css/elegant-icons-style.css" rel="stylesheet" />
  <link href="css/font-awesome.min.css" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="assetss/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="assetss/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
  <link href="css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="css/fullcalendar.css">
  <link href="css/widgets.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet" />
  <link href="css/xcharts.min.css" rel=" stylesheet">
  <link href="css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  <script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript" src="js/jquery.easy-ticker.js"></script>

</head>
	<!-- Slider Script Start -->
<script type="text/javascript">
$(document).ready(function(){

	var dd = $('.vticker').easyTicker({
		direction: 'up',
		easing: 'easeInOutBack',
		speed: 'slow',
		interval: 2000,
		height: '250px',
		visible: 1,
		mousePause: 0,
		controls: {
			up: '.up',
			down: '.down',
			toggle: '.toggle',
			stopText: 'Stop !!!'
		}
	}).data('easyTicker');
	
	cc = 1;
	$('.aa').click(function(){
		$('.vticker ul').append('<li>' + cc + ' Triangles can be made easily using CSS also without any images. This trick requires only div tags and some</li>');
		cc++;
	});
	
	$('.vis').click(function(){
		dd.options['visible'] = 3;
		
	});
	
	$('.visall').click(function(){
		dd.stop();
		dd.options['visible'] = 0 ;
		dd.start();
	});
	
});
</script>
<body>
 
  <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1 style=""><strong>Youde</strong></h1>
                            <div class="description">
                            	
                            </div>
                        </div>
                    </div>
     <div class="col-lg-6 col-lg-offset-3 ">
         <div id="userform">
           <ul class="nav nav-tabs nav-justified" role="tablist">
              <li class="active"><a href="#signup"  role="tab" data-toggle="tab">Member Login</a></li>
               <li><a href="#login"  role="tab" data-toggle="tab">Log in</a></li>
           </ul>
            <div class="tab-content">
              <div class="tab-pane fade active in" id="signup">
                 <h2 class="text-uppercase text-center"> <strong>Member Login</strong></h2>
                     <form id="signup">
                         <div class="row">
                            <div class="col-xs-12 col-sm-6">
                               <div class="form-group">
                                  <label>First Name<span class="req">*</span> </label>
                                  <input type="text" class="form-control" id="first_name" required data-validation-required-message="Please enter your name." autocomplete="off">
                                  <p class="help-block text-danger"></p>
                             </div>
                             </div>
<div class="col-xs-12 col-sm-6">

</div>
</div>
<div class="form-group">
<label> Your Email<span class="req">*</span> </label>
<input type="email" class="form-control" style="width:255px;" id="email" required data-validation-required-message="Please enter your email address." autocomplete="off">
<p class="help-block text-danger"></p>
</div>


<div class="mrgn-30-top">
<button type="submit" class="btn btn-larger btn-block"/>
Login
</button>
</div>
</form>
</div>
<div class="tab-pane fade in" id="login">

<h2 class="text-uppercase text-center">Admin Login</h2>
<form id="login">
<div class="form-group">
<label> Your Email<span class="req">*</span> </label>
<input type="email" class="form-control" id="email" required data-validation-required-message="Please enter your email address." autocomplete="off">
<p class="help-block text-danger"></p>
</div>
<div class="form-group">
<label> Password<span class="req">*</span> </label>
<input type="password" class="form-control" id="password" required data-validation-required-message="Please enter your password" autocomplete="off">
<p class="help-block text-danger"></p>
</div>
<div class="mrgn-30-top">
<button type="submit" class="btn btn-larger btn-block"/>
 Login
</button>
</div>
</form>
</div>
</div>
</div>
</body>
</html>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    
        <script src="js/index.js"></script>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript" src="js/jquery.easy-ticker.js"></script>
