<?php    
include("connection.php");    
session_start();
$oid=$_REQUEST['oid'];
if(isset($_SESSION['sess_id'])){
$sid=$_SESSION['sess_id'];
}else{
	header("location:index.php");
}
?>


<!doctype html>
<html>
<head>
<title>Homepage</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/nav.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/script.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>

<style type="text/css">
	#content
		{
		 
		   width:100%;
		   padding-top:20px;
		  border-top:1px solid #d3d3d3;
		  float:left;
			
		}
		#detail th {
			width:300px;
			padding:15px;
			text-align:left
		}
		
		#detail td {
			width:600px;
			padding:15px;
			text-align:left;
		}
		#sidebar
		{

	      height:800px;
		   width:250px;
		   background-color:#ffffff;
		   margin-left:5px;
		   border:1px solid #d3d3d3;
		   float:left;
		}
	
		#menu li.mp
		{
			border-radius:5px;
			margin-bottom:10px;
					background-color:#dbdbdb;
			text-align:center;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.ep
		{
			border-radius:5px;
					background-color:#dbdbdb;
			text-align:center;
			margin-bottom:10px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
	    #menu li.mo
		{
			border-radius:5px;
			text-align:center;
					background-color:#dbdbdb;
			margin-bottom:10px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.lg
		{
			border-radius:5px;
			text-align:center;
		 background-color:#dbdbdb;
		    padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu ol
		{
		
		   margin-top:30px;
		  list-style:none;
		  letter-spacing:2px;
		
		}
		#menu a
		{
		  display:inline-block;
		  padding:6px;
		 text-decoration: none;
		}
.od
{
  margin:15px;
  float:left;
}
table{

	border:1px solid black;
    
}

th
{
	
	padding:15px;
}
td{
   padding:8px;	
}
.h
		{
		
			text-align:center;
			font-size:16pt;
		    padding:5px;
			margin-bottom:20px;
		}


</style>
<SCRIPT LANGUAGE="JavaScript"> 
if (window.print) {

}
</script>
</head>
<body>
    <div id="wrapper">

	    <?php include_once("header.php");?>
	<div id="content">
      <div id="sidebar">
		       <div id="menu">
			            <div class="h">
				<?php $result=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
				$row2=mysqli_fetch_array($result);?>
		                <b><?php echo $row2['Agent_UserName'];?></b>
				
					 </div>
	
		
		


			     <ol>
	            <li class="mp"><a href="member_profile.php">Member profile</a></li>
			    <li class="ep"><a href="member_edit.php">Edit Profile</a></li>
			    <li class="mo"><a href="changepassword.php">Change Password</a></li>
				<li class="mo"><a href="vieworder.php">My Orders</a></li>
				<li class="mo"><a href="orderhistory.php">Order History</a></li>
			    <li class="lg"><a href="logout.php">Logout</a></li>
			   </ol>
			  </div>
	   </div>
		   <div class="od">
	          <div class="ho">
		       <h2>My Orders</h2>
		      </div>
	
	    
           <?php
		$result2=mysqli_query($conn,"SELECT * FROM cus_order WHERE Order_ID=$oid");
		while($row2=mysqli_fetch_array($result2))
		{	$result=mysqli_query($conn,"SELECT * FROM cus_order, orderdetail WHERE cus_order.Order_ID=$oid AND orderdetail.Order_ID=$oid");
			$row=mysqli_fetch_array($result);
		?>
			<p> Order ID : <?php echo $row['Order_ID'] ?>
			</p>
			<p> Order Date : <?php echo $row['Order_Date']?>
			</p>
			<p>	Order Status : <?php echo $row['Order_Status']?>
			</p>
			<p> Ship Name : <?php echo $row['Ship_Name']?>
			</p>
			<p> Ship Phone Number : 0<?php echo $row['Ship_Contactnumber']?>
			</p>
			<p> Ship Address : <?php echo $row['Ship_Address']."\n".$row['Ship_Postcode'].",".$row['Ship_City']."\n".$row['Ship_State'] ;?>
			</p>
			<?php
			$result3=mysqli_query($conn,"SELECT * FROM cus_order, orderdetail WHERE cus_order.Order_ID=$oid AND orderdetail.Order_ID=$oid");
			while($row3=mysqli_fetch_array($result3))
			{	$pid=$row3['Product_ID'];
				$result4=mysqli_query($conn, "SELECT * FROM product WHERE Product_ID=$pid");
				$row4=mysqli_fetch_array($result4)
				?>
				<p> Product Name : <?php echo $row4['Product_Name'];?>
				</p>
				<p> Product Price : <?php echo $row4['Product_Price'];?>
				</p>
				<p> Order Unit : <?php echo $row3['Order_Quantity'];?>
				</p>
				<p> Order Price : <?php echo $row3['Order_Price'];?>
				</p>
			<?php
			}	
		}?>
        <div id="pp">
		<FORM>
         <INPUT TYPE="button" onClick="window.print()" value="Save&print">
       </FORM>
		</div>

	</div>
  </div>
</div>
	<?php include_once("footer.php");?>    

</body>
</html>	