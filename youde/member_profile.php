<?php    
include("connection.php");    

?>
<!DOCTYPE HTML>
<html>
<head><title>Member Profile</title>
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/nav.css"/>
<link rel="shortcut icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/homepage/favicon.ico" type="image/x-icon">
<link href="css/modern-business.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/script.js"></script>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.3.js"></script>
<style type="text/css">
	#btnRegister input[type="submit"]{
			color:white;
			width:100px;
			font-size:10pt;
			background-color:orange;
			border:1px solid rgb(234,120,0);
			border-radius:5px;
			font-weight:bold;
			font-family:Nirmala UI;
			opacity:0.75;
		}		
		
		#btnEdit input[type="submit"] {
			color:white;
			width:80px;
			font-size:10pt;
			background-color:orange;
			border:1px solid rgb(234,120,0);
			border-radius:5px;
			font-weight:bold;
			font-family:Nirmala UI;
			opacity:0.75;
		}		
		
		#btnEdit singkeongyainput[type="submit"]:hover {
			opacity:1;
		}
		
		legend {
		    font-size:14px;
			 color: #4c4c4c;
			font-weight:bold;
			 background: #d9d9d9;
			 float:left;
			margin-top:5px;
			padding:5px;
		}
		#content
		{width:100%;
	
		  float:left;}
		#detail th {
			width:300px;
			padding:15px;
			text-align:left
		}
		
		#detail td {
			width:600px;
			padding:15px;
			text-align:left;
		}
		#sidebar
		{

	      height:800px;
		   width:250px;
		   background-color:#ffffff;
		   margin-left:5px;
		   border:1px solid #d3d3d3;
		   float:left;
		}
	
		#menu li.mp
		{
			border-radius:5px;
			margin-bottom:16px;
			background-color:#dbdbdb;
			text-align:center;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.ep
		{
			border-radius:5px;
			background-color:#dbdbdb;
			text-align:center;
			margin-bottom:16px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
	    #menu li.mo
		{
			border-radius:5px;
			text-align:center;
			background-color:#dbdbdb;
			margin-bottom:16px;
			padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu li.lg
		{
			border-radius:5px;
			text-align:center;
			background-color:#dbdbdb;
		    padding:3px;
			width:180px;
			height:45px;
			border:1px solid #d3d3d3;
		}
		#menu ol
		{
		
		   margin-top:30px;
		  list-style:none;
		  letter-spacing:2px;
		
		}
		#menu a
		{
		  display:inline-block;
		  padding:6px;
		 text-decoration: none;
		}
		.form
		{
          padding-left:20px;
		 float:left;
		}
		#pic
		{
		   width:200px;
		   height:200px;
		   margin-left:20px;
		}
		.h
		{
		
			text-align:center;
			font-size:16pt;
		    padding:5px;
			margin-bottom:20px;
		}
	</style>

</head>
<body>
     <?php include_once("header.php");?>
	<div id="content">
      <div id="sidebar">
		       <div id="menu">
			         <div class="h">
				<?php $result=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
				$row2=mysqli_fetch_array($result);?>
		                <b><?php echo $row2['Agent_Name'];?></b>
				
					 </div>
	             <ol>
                <li class="mp"><a href="member_profile.php">Member profile</a></li>
			    <li class="ep"><a href="member_edit.php">Edit Profile</a></li>
			    <li class="mo"><a href="changepassword.php">Change Password</a></li>
				<li class="mo"><a href="vieworder.php">My Orders</a></li>
				<li class="mo"><a href="orderhistory.php">Order History</a></li>
			    <li class="lg"><a href="logout.php">Logout</a></li>
			   </ol>
			  </div>
	   </div>
		
			     <div class="form">
						<fieldset><legend>Personal Information</legend>
						<div style="padding:20px;font-family:verdana;font-size:12pt">
							<table id="detail">
								<tr>
								<?php $result=mysqli_query($conn, "SELECT * FROM agent WHERE Agent_Id = $sid");
										$row2=mysqli_fetch_array($result);?>
									<th>Name</th>
									<td><?php echo $row2['Agent_Name'];?></td>
								</tr>
								<tr>
									<th>Email</th>
									<td><?php echo $row2['Agent_Email']; ?></td>
								</tr>
								<tr>
									<th>Contact Number</th>
									<td><?php echo $row2['Agent_Phonenumber']; ?></td>
								</tr>
								<tr>	
									<th>Brithdate</th>
									<td><?php echo $row2['Agent_Birthday']; ?></td>
							
								</tr>
								<tr>
									<th>Address	</th>
									<td><?php echo $row2['Agent_Address']; ?></td>
								</tr>
								<tr>
								     <th>City	</th>
									<td><?php echo $row2['Agent_City']; ?></td>
								</tr>
								<tr>
									<th>Postcode</th>
									<td><?php echo $row2['Agent_Postcode']; ?></td>
								</tr>
								<tr>
							     <th>State</th>
									<td><?php echo $row2['Agent_State']; ?></td>
								</tr>
							
							</table>
						</div>	
					</fieldset>
			   </div>
	     </div>	
		 <?php include_once("footer.php");?>
</body>
</html>
