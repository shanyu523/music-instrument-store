<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Material Dashboard by Creative Tim</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="../assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
<div class="logo">
                <a href="" class="simple-text">
                    Youde
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active">
                        <a href="dashboard.html">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="./user.html">
                            <i class="material-icons">person</i>
                            <p>Network</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">View Network</a>
                                </li>
                            
                            </ul>
                    </li>
                    <li>
                        <a href="./table.html">
                            <i class="fa fa-user"></i>
                        <a href="addmember.php">Add Member</a>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="fa fa-money"></i>
                            <p>E-wallet</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Register wallet</a>
                                </li>
                                <li>
                                    <a href="morris.html">View wallet</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="fa fa-shopping-cart"></i>
                            <p>E-commerce</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Product</a>
                                </li>
                                <li>
                                    <a href="morris.html">Edit Product</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                  
                    <li class="active-pro">
                        <a href="upgrade.html">
                            <i class="material-icons"></i>
                            <p></p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Admin </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Add Member</h4>
                                    <p class="category">
                                        <a target="_blank" href="https://design.google.com/icons/"></a>
                                    </p>
                                </div>
                                <div class="card-content">
                                    <div class="iframe-container hidden-sm hidden-xs">
                           

  <form action="/action_page.php"  style="margin-left:210px;"  >
    <div class="form-group">
      <label for="email"> <b>Product ID:</b></label>
      <input type="from" class="form-control" id="from" placeholder="Enter email" name="email" style=" width:250px;">
    </div>
	    <div class="form-group">
      <label for="pwd"><b>Prodcut Name:</b></label>
      <input type="password" class="form-control" id="to" placeholder="Enter password" name="pwd" style=" width:250px;">
    </div>
    <div class="form-group">
      <label for="pwd"><b>Category:</b></label>
      <input type="password" class="form-control" id="to" placeholder="Enter password" name="pwd" style=" width:250px;">
    </div>
	<div class="form-group">
      <label for="pwd"><b>Product PV:</b></label>
      <input type="password" class="form-control" id="balance" placeholder="Enter password" name="pv" style="width:250px;">
    </div>
	<div class="form-group">
      <label for="pwd"><b>Product Discount:</b></label>
      <input type="password" class="form-control" id="description" placeholder="Enter password" name="discount" style="width:250px;">
    </div>
		<div class="form-group">
      <label for="pwd"><b>Product Description:</b></label>
      <textarea class="form-control" rows="4" id="comment" style="width:250px;" name="description"></textarea>
    </div>
       <div class="form-group">
      <label for="pwd"><b>Product Quantity:</b></label>
        <input type="password" class="form-control" id="description" placeholder="Enter Quantity" name="prod_qty" style="width:250px;">
    </div>
	   <div class="form-group"style="width:200px;">
      <label for="pwd" ><b>Product Status:</b></label>
        	<p><select name="prod_status" class="form-control">
		    <option value="Available">Available</option>
			<option value="New">New</option>
			<option value="Sales">Sales</option>
		</select></p>
    </div>
     <div class="field">
  <label for ="images1">Product Image (1):</label>
	<p> <input name="fileToUpload1" id="fileToUpload1" type="file" size="5"  class="er"/></p>
  </div>
  <div class="field">
    <label for ="images2">Product Image (2):</label>
	<p> <input name="fileToUpload2" id="fileToUpload2" type="file" size="5"  class="er"/></p>
  </div>
    <div class="field">
    <label for ="images3">Product Image (3):</label>
	<p> <input name="fileToUpload3" id="fileToUpload2" type="file" size="5"  class="er"/></p>
  </div>
  <div class="" style=" margin-left:300px;">
    <button type="submit" class="btn btn-default" style="">Submit</button>
	</div>
  </form>
                                    </div>
                                    <div class="col-md-6 hidden-lg hidden-md text-center">
                                        <h5>The icons are visible on Desktop mode inside an iframe. Since the iframe is not working on Mobile and Tablets please visit the icons on their original page on Google. Check the
                                            <a href="https://design.google.com/icons/" target="_blank">Material Icons</a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="../assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="../assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="../assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>

</html>