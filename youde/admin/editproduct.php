<?php include("connection.php");
session_start();
$pid=$_REQUEST['proid'];
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Material Dashboard by Creative Tim</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="../assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
<div class="logo">
                <a href="" class="simple-text">
                    Youde
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active">
                        <a href="dashboard.html">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="./user.html">
                            <i class="material-icons">person</i>
                            <p>Network</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">View Network</a>
                                </li>
                            
                            </ul>
                    </li>
                    <li>
                        <a href="./table.html">
                            <i class="fa fa-user"></i>
                            <p>Add Member</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="fa fa-money"></i>
                            <p>E-wallet</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Register wallet</a>
                                </li>
                                <li>
                                    <a href="morris.html">View wallet</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="fa fa-shopping-cart"></i>
                            <p>E-commerce</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Product</a>
                                </li>
                                <li>
                                    <a href="morris.html">Edit Product</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                  
                    <li class="active-pro">
                        <a href="upgrade.html">
                            <i class="material-icons"></i>
                            <p></p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Admin </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <?php include("viewnotification.php"); ?>
                                </ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Edit Product</h4>
                                    <p class="category">
                                        <a target="_blank" href="https://design.google.com/icons/"></a>
                                    </p>
                                </div>
                                <div class="card-content">
                                    <div class="iframe-container hidden-sm hidden-xs">
                           

  <form action=""  style="margin-left:210px;"  >
    <div class="form-group">
	<?php $result=mysqli_query($conn, "SELECT * FROM product WHERE Product_ID=$pid");
	$row=mysqli_fetch_array($result);?>
      <label for="email"> <b>Product ID :</b></label><?php echo $row['Product_ID'];?>
      
    </div>
	    <div class="form-group">
      <label for="pwd"><b>Prodcut Name:</b></label>
      <input name="prod_name" value="<?php echo $row['Product_Name'];?>" type="text" size="25" class="form-control" required />
    </div>
    <div class="form-group">
      <label for="pwd"><b>Category:</b></label>
      <select name="category" class="form-control">
	<?php	$result2 = mysqli_query($conn, "SELECT * FROM category");
			while($row2=mysqli_fetch_array($result2))
			{
				?><optgroup label="<?php echo $row2['Categoty_Name'];?>">
				<?php 
				$cid = $row2['Category_ID'];
				$result3 = mysqli_query($conn,"SELECT * FROM subcategory WHERE Category_ID= $cid");
				while($row3 = mysqli_fetch_array($result3))
				{
					?><option value="<?php echo $row3['Subcategory_ID'];?>"><?php echo $row3['Subcategory_Name'];?></option>
					<?php
				}
				?></optgroup><?php
			}
			?>
		</select>    </div>
	<div class="form-group">
      <label for="price"><b>Product PV:</b></label>
      <input type="text" value="<?php echo $row['Product_Point'];?>" name="price" class="form-control" size="20" required>
    </div>
	<div class="form-group">
      <label for="pwd"><b>Product Discount:</b></label>
      <input type="text" value="<?php echo $row['Product_Discount'];?>" name="discount" class="form-control" size="20" required>
    </div>
		<div class="form-group">
      <label for="pwd"><b>Product Description:</b></label>
     <textarea name="detail" value="<?php echo $row['Product_Description'];?>" class="form-control" required><?php echo $row['Product_Description'];?></textarea>
    </div>
       <div class="form-group">
      <label for="pwd"><b>Product Quantity:</b></label>
        <input value="<?php echo $row['Product_Quantity'];?>" name="prod_qty" type="text" size="10" value="" class="form-control" required />
    </div>
	   <div class="form-group"style="width:200px;">
      <label for="pwd" ><b>Product Status:</b></label>
	  <select name="prod_status" class="form-control" >
        	<?php $s1='';
			$s2='';
			if($row['Product_Status']=='Available')
			{
				$s1='Discount';
				$s2='Delete';
			}
			else if($row['Product_Status']=='Discount')
			{
				$s1='Available';
				$s2='Delete';
			}
			else if($row['Product_Status']=='Delete')
			{
				$s1='Available';
				$s2='Discount';
			}
			?>
			<option value="<?php echo $row['Product_Status'];?>"><?php echo $row['Product_Status'];?></option>
			<option value="<?php echo $s1;?>"><?php echo $s1;?></option>
			<option value="<?php echo $s2;?>"><?php echo $s2;?></option>
			
		</select></p>
    </div>
	<?php $iid=$row['Images_ID'];
	$result4=mysqli_query($conn,"SELECT * FROM image WHERE Images_ID='$iid'");
	$row4=mysqli_fetch_array($result4);
	?>
     <div class="field">
  <label for ="images1">Product Image (1):</label>
	<p> <input name="fileToUpload1" value="<?php echo $row4['Images_1'];?>" id="fileToUpload1" type="file" size="5"  class="er"/></p>
  </div>
  <div class="field">
    <label for ="images2">Product Image (2):</label>
	<p> <input name="fileToUpload2" value="<?php echo $row4['Images_2'];?>" id="fileToUpload2" type="file" size="5" class="er" /></p>
  </div>
    <div class="field">
    <label for ="images3">Product Image (3):</label>
	<p> <input name="fileToUpload3" value="<?php echo $row4['Images_3'];?>" id="fileToUpload3" type="file" size="5"  class="er"/></p>
  </div>
  <p>	Click If Wan Update Images : <input type="submit" name="imgadd"  class="btn btn-default" value="Update Images"/> (* If wan change images location with no click will not be add)
</p>
  <div class="" style=" margin-left:300px;">
    <button name="ADD" value="ADD" type="submit" class="btn btn-default" style="">Submit</button>
	</div>
  </form>
                                    </div>
							<?php

$t=time();
$time = date("Y-m-d",$t);
$addtime=$row['Product_AddDate'];
$wan="false";
	if(isset($_POST['imgadd']))
	{
		$wan="true";
		
		$images1 = $_POST["fileToUpload1"];
		$images2 = $_POST["fileToUpload2"];
		$images3 = $_POST["fileToUpload3"];
		if($wan="true")
		{
			if(mysqli_query($conn, "UPDATE 'image' SET 
		'Images_1'='$images1',
		'Images_2'='$images2',
		'Images_3'='$images3'
		WHERE Images_ID=$iid"))
			{
				$upimg="true";
			}
		}
	}
	
	if(isset($_POST['ADD']))
	{
		$upimg="false";
		$upprod="false";
		$prod_name = $_POST["prod_name"];
		$sub_category = $_POST["category"];
		$row6 = mysqli_fetch_array(mysqli_query($conn, "SELECT 'Category_ID' FROM 'subcategory' WHERE  Subcategory_ID=$sub_category"));
		$category = $row6['Category_ID'];
		$price = $_POST["price"];
		$discount = $_POST["discount"];
		$detail = $_POST["detail"];
		$feature = $_POST["feature"];
		$prod_qty = $_POST["prod_qty"];
		$prod_status = $_POST["prod_status"];
		
		
		
		if($wan="true")
		{
			if(mysqli_query($conn, "UPDATE 'product' SET 
			'Product_Name'='$prod_name',
			'Product_Point'='$price',
			'Product_Discount'='$discount',
			'Product_Description'='$detail',
			'Product_Quantity'='$prod_qty',
			'Product_Status'='$prod_status',
			'Product_UpdateDate'='$time',
			'Product_AddDate'='$addtime',
			'Category_ID'='$category',
			'Subcategory_ID'='$sub_category',
			'Images_ID'='$iid' 
			WHERE Product_ID = '$pid'"))
			{
				$upprod="true";
			}
		}
		else
		{
			if(mysqli_query($conn, "UPDATE 'product' SET 
			'Product_Name'='$prod_name',
			'Product_Point'='$price',
			'Product_Discount'='$discount',
			'Product_Description'='$detail',
			'Product_Quantity'='$prod_qty',
			'Product_Status'='$prod_status',
			'Product_UpdateDate'='$time',
			'Product_AddDate'='$addtime',
			'Category_ID'='$category',
			'Subcategory_ID'='$sub_category',
			WHERE Product_ID = '$pid'"))
			{
				$upprod="true";
			}
		}
		if($wan="true")
		{
			if($upimg="true"&& $upprod="true")
			{
				?><script type="text/javascript">alert("UPDATE Successful!");
				header("location:view_product.php");</script><?php
				
			}
			else
			{
				?><script type="text/javascript">alert("UPDATE False!");
				</script><?php
			}
		}
		else
		{
			if($upprod=="true")
			{
				?><script type="text/javascript">alert("UPDATE Successful!");
				window.location("view_product.php");
				</script><?php
			}
			else
			{
				?><script type="text/javascript">alert("UPDATE False!");
				</script><?php
			}
		}
	}
        
?>		
                                    <div class="col-md-6 hidden-lg hidden-md text-center">
                                        <h5>The icons are visible on Desktop mode inside an iframe. Since the iframe is not working on Mobile and Tablets please visit the icons on their original page on Google. Check the
                                            <a href="https://design.google.com/icons/" target="_blank">Material Icons</a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="">Youde</a>, made with love for a better web
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="../assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="../assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="../assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>

</html>
<script>
window.location("view_product.php");
</script>
<script>
 $('document').ready(function()
	  { 
	      
          $("#addproduct").validate({
		         errorPlacement: function (error, element) {
            error.insertAfter(element);
            if (element.hasClass('form-control')) {
                element.next().removeClass('passValid').addClass('passError');
            }
        },
		success: function (label) {
            if (label.prev().hasClass('form-control')) {
			   
			   label.text('ok!').addClass("checked");
			
            }
        },

			    rules: {
                    productname:{
					  required:true,
				      lettersonly:true
					  },
            
                    category: {
                        required: true,
						 valueNotEquals:"default"
                       },
                    producttype: {
                        required: true,
						 valueNotEquals:"default"
                    
                    },
					price: {
			               required:true,
                           price:true
                    },
					detail:{
					     required:true
						
					},
					images1:{
						required:true

					},
					images2:{
						required:true
		
				    },
					images3:{
						required:true
				
					},
				
                },
                messages: {
                    productname: { 
					    required:"Please Enter Product Name",
						lettersonly:"Please enter lettersonly"
				     },
	                category: {
                        required: "Please Choose Category",
                        valueNotEquals: "Please choose your state!"
                    },
					producttype:{
						    required:"Please Choose Product Type",
						    valueNotEquals: "Please choose your state!"
					},
                    price: {
						 required:"Please Enter Price",
						 price:"Invalid Format"
	                      },
				    detail: {
						 required:"Please Enter product description"
				
						},
				   images1:{
					  required:"Please upload images"
					  },
				   images2:{
					  required:"Please upload images"
			
				  },
				  images3:{
					  required:"Please upload images"
				
				  },
				state:{
					  required:"Please choose your state",
					  valueNotEquals: "Please choose your state!"
					
				  },
                },
                submitHandler: function(form) {
                    form.submit();
					$( '#addproduct').each(function(){
                     this.reset();
					});
                }
			
            }); 
   	   
     jQuery.validator.addMethod("num", function(value, element) {
    return this.optional(element) || /^6?01\d{8}$/.test(value);
     }, "Letters only please"); 
     jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value);
     }, "");
	  jQuery.validator.addMethod("price", function(value, element) {
    return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/.test(value);
     }, "");
    jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg != value;
   }, "Value must not equal arg.");

	
});
</script>


