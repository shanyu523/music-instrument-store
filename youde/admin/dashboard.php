<?php include ("connection.php");
//if(isset($_GET['Login_Button']))
//{
//	$Login_Button = $_GET['Login_Button'];
//	$sql = "select * From admin WHERE Admin_Id ='$Login_Button' ";
//	$result = mysqli_query ($conn, $sql);
//	$row = mysqli_fetch_assoc($result);
//	$_SESSION['aid'] = $Login_Button;
//	}
//	else{
//	header("location:adminlogin.php");
//}
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Material Dashboard by Creative Tim</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="../assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo">
                <a href="" class="simple-text">
                    Youde
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active">
                        <a href="dashboard.php">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="./user.html">
                            <i class="material-icons">person</i>
                            <p>Network</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">View Network</a>
                                </li>
                            
                            </ul>
                    </li>
                    <li>
                        <a href="register.php">
                            <i class="fa fa-user"></i>
                            <p>Add Member</p>
                        </a>
                    </li>
                    <li>
                        <a href="register.php">
                            <i class="fa fa-money"></i>
                            <p>E-wallet</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Register wallet</a>
                                </li>
                                <li>
                                    <a href="morris.html">View wallet</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="fa fa-shopping-cart"></i>
                            <p>E-commerce</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="addproduct.php">Add Product</a>
                                </li>
                                <li>
                                    <a href="viewproduct.php">Edit Product</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                  
                    <li class="active-pro">
                        <a href="upgrade.html">
                            <i class="material-icons"></i>
                            <p></p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> <b>Admin</b> </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
                                   <?php include("viewnotification.php"); ?>
                                </ul>
                            </li>
                            <li>
                                <a href="viewprofile.php">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
          
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                 
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="green" style="margin:5px">
                                 <div class="card-content">
                                    <h4 class="title">Total Product</h4>
                                     <?php
							$result=mysqli_query($conn, "SELECT * FROM product WHERE Product_Status!='Delete'");
							$rowcount=mysqli_num_rows($result);
							?>
							<a href="viewproduct.php" style="font-weight:600;color:black;"><?php echo $rowcount;?></a><?php
						 ?>
						 <p> </p>
						 <h4 class="title">Total Agent</h4>
                                     <?php
							$result=mysqli_query($conn, "SELECT * FROM agent ");
							$rowcount=mysqli_num_rows($result);
							?>
							<a href="viewmember.php" style="font-weight:600;color:black;"><?php echo $rowcount;?></a><?php
						 ?>
                                </div>
                                </div>
                                
                          
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="orange" style="margin:5px">
                                    
                             
                                <div class="card-content">
                                    <h4 class="title">Total Sales</h4>
                                   <?php
							$result2=mysqli_query($conn, "SELECT * FROM orderdetail");
							$total = 0;
							while($row2=mysqli_fetch_array($result2))
							{
								$total+=$row2['Order_Price'];
							}
							?>
				            <a href="vieworder.php" style="font-weight:600;color:black;"><b><?php echo $total;?></b></a><?php
						  ?>
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-12">
                                <div class="card" style="width:780px; ">
                                <div class="card-header" data-background-color="orange" style="width:780px;">
                                    <h4 class="title">Product List</h4>
                                 
                                </div>
                                <div class="card-content table-responsive" >
                                    <table class="table table-hover">
                                        <tr>
						<th width="70px" ><p>Product ID</p></th>
						<th width="200px"><p>Product Name</p></th>
						<th width="100px"><p>Product Point</p></th>
						<th width="150px"><p>Product Discount</p></th>
						<th><p>Product Quantity</p></th>
						
					</tr>
					<?php
					$result4 = mysqli_query($conn, "SELECT * FROM product");	
					$rowcount4=mysqli_num_rows($result4);
					if($rowcount4<=0)
					{
						?>
						<tr><td colspan="5">NO RECORD</td></tr>
						<?php
					}
					else
					{
					while($row4=mysqli_fetch_array($result4))
					{
						
						$result5=mysqli_query($conn, "SELECT * FROM product");
						$row5=mysqli_fetch_array($result5);
						?>
					<tr>
						<td><p><a href="viewproduct.php?oid=<?php echo $row5['Product_ID'];?>"><?php echo $row4['Product_ID'];?></a></p></td>
						<td><p><?php echo $row5['Product_Name'];?></p></td>
						<td><p><?php echo $row4['Product_Point'];?></p></td>
						<td><p><?php echo $row4['Product_Discount'];?></p></td>
						<td><p><?php echo $row5['Product_Quantity'];?></p></td>
					</tr>
					<?php
					}
					}
					?>
                                    </table>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="card" style="width:780px;">
                                <div class="card-header" data-background-color="orange" style="width:780px;">
                                    <h4 class="title">Order List</h4>
                                 
                                </div>
                                <div class="card-content table-responsive" >
                                    <table class="table table-hover">
                                        <tr>
						<th width="70px" ><p>Order ID</p></th>
						<th width="200px"><p>Customer Name</p></th>
						<th width="100px"><p>Order Date</p></th>
						<th width="150px"><p>Status</p></th>
						<th><p>Total(RM)</p></th>
						
					</tr>
					<?php
					$result4 = mysqli_query($conn, "SELECT * FROM cus_order WHERE Order_Status!='Arrived' ORDER BY 'Order_ID' DESC limit 5");	
					$rowcount4=mysqli_num_rows($result4);
					if($rowcount4<=0)
					{
						?>
						<tr><td colspan="5">NO RECORD</td></tr>
						<?php
					}
					else
					{
					while($row4=mysqli_fetch_array($result4))
					{
						$id=$row4['Agent_Id'];
						$oid=$row4['Order_ID'];
						$result5=mysqli_query($conn, "SELECT * FROM `orderdetail`, `agent` WHERE agent.Agent_Id=$id AND orderdetail.Order_ID=$oid");
						$row5=mysqli_fetch_array($result5);
						?>
					<tr>
						<td><p><a href="vieworder.php?oid=<?php echo $row5['Agent_Id'];?>"><?php echo $row4['Order_ID'];?></a></p></td>
						<td><p><?php echo $row5['Agent_Name'];?></p></td>
						<td><p><?php echo $row4['Order_Date'];?></p></td>
						<td><p><?php echo $row4['Order_Status'];?></p></td>
						<td><p><?php echo $row5['Order_Price'];?></p></td>
					</tr>
					<?php
					}
					}
					?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Portfolio
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="">Youde</a>
						
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="../assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="../assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="../assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        // Javascript method's body can be found in assets/js/demos.js
        demo.initDashboardPageCharts();

    });
</script>
<script type="text/javascript">
	
	 var $table = $('#table');
		     $table.bootstrapTable({
			      url: 'list-user.php',
			      search: true,
			      pagination: true,
			      buttonsClass: 'primary',
			      showFooter: true,
			      minimumCountColumns: 3,
			      columns: [{
			          field: 'No.',
			          title: 'Agent ID',
			          sortable: true,
			      },{
			          field: 'Agent Name',
			          title: 'Agent Name',
			          sortable: true,
			      },{
			          field: 'Agent Address',
			          title: 'Agent Address',
			          sortable: true,
			      },{
					  ],
 
  			 });

</script>
</html>