<?php
 include("connection.php") ;
session_start();

    $result = mysqli_query ( $conn, "SELECT * FROM `product` ORDER BY Product_ID DESC");
	$row = mysqli_fetch_assoc($result);
	$rowcount = mysqli_num_rows($result);
	$nextid=$row['Product_ID']+1;
?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Material Dashboard by Creative Tim</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="../assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
<div class="logo">
                <a href="" class="simple-text">
                    Youde
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="active">
                        <a href="dashboard.php">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li>
                        <a href="./user.html">
                            <i class="material-icons">person</i>
                            <p>Network</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">View Network</a>
                                </li>
                            
                            </ul>
                    </li>
                    <li>
                        <a href="./table.html">
                            <i class="fa fa-user"></i>
                            <p>Add Member</p>
                        </a>
                    </li>
                    <li>
                        <a href="./typography.html">
                            <i class="fa fa-money"></i>
                            <p>E-wallet</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Register wallet</a>
                                </li>
                                <li>
                                    <a href="morris.html">View wallet</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./icons.html">
                            <i class="fa fa-shopping-cart"></i>
                            <p>E-commerce</p>
                        </a>
						<ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Add Product</a>
                                </li>
                                <li>
                                    <a href="morris.html">Edit Product</a>
                                </li>
                            </ul>
                    </li>
                    <li>
                        <a href="./maps.html">
                            <i class="fa fa-sign-out"></i>
                            <p>Logout</p>
                        </a>
                    </li>
                  
                    <li class="active-pro">
                        <a href="upgrade.html">
                            <i class="material-icons"></i>
                            <p></p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Admin </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                </a>
                                <ul class="dropdown-menu">
										<?php include("viewnotification.php"); ?>
								</ul>
                            </li>
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                        </ul>
                       
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Add Product</h4>
                                    <p class="category">
                                        <a target="_blank" href="https://design.google.com/icons/"></a>
                                    </p>
                                </div>
                                <div class="card-content">
                                    <div class="iframe-container hidden-sm hidden-xs">
                           

  <form id="addproduct" method="post"b name="addproduct" action=""  style="margin-left:210px;"  >
    <div class="form-group">
      <label for="productname"> <b>Product ID:</b></label>
       &nbsp <?php echo $nextid ?>
    </div>
	    <div class="form-group">
      <label for="productname"><b>Prodcut Name:</b></label>
      <input type="text" class="form-control" id="to" placeholder="Enter Name" name="productname" style=" width:250px;" required>
    </div>
    <div class="form-group">
      <label for="category"><b>Category:</b></label>
	  <p><select name="category" class="form-control">
	  <?php	$result2 = mysqli_query($conn, "SELECT * FROM category");
			while($row2=mysqli_fetch_array($result2))
			{
				?><optgroup label="<?php echo $row2['Categoty_Name'];?>">
				<?php 
				$pid = $row2['Category_ID'];
				$result3 = mysqli_query($conn,"SELECT * FROM subcategory WHERE Category_ID= $pid");
				while($row3 = mysqli_fetch_array($result3))
				{
					?><option value="<?php echo $row3['Subcategory_ID'];?>"><?php echo $row3['Subcategory_Name'];?></option>
					<?php
				}
				?></optgroup><?php
			}
			?>
			</select></p>
    </div>
	<div class="form-group">
      <label for="price"><b>Product PV:</b></label>
      <input type="text" class="form-control" id="balance" placeholder="Enter PV" name="price" style="width:250px;" required>
    </div>
	<div class="form-group">
      <label for="price"><b>Product Discount:</b></label>
      <input type="text" class="form-control" id="description" placeholder="Enter Discount" name="discount" style="width:250px;" required>
    </div>
		<div class="form-group">
      <label for="detail"><b>Product Description:</b></label>
      <textarea class="form-control" rows="4" id="comment" style="width:250px;" name="detail" required></textarea>
    </div>
       <div class="form-group">
      <label for="feature"><b>Product Quantity:</b></label>
        <input type="text" class="form-control" id="description" placeholder="Enter Quantity" name="prod_qty" style="width:250px;" required>
    </div>
	   <div class="form-group"style="width:200px;">
      <label for="feature" ><b>Product Status:</b></label>
        	<p><select name="prod_status" class="form-control">
		    <option value="Available">Available</option>
			<option value="New">New</option>
			<option value="Sales">Sales</option>
		</select></p>
    </div>
     <div class="field">
  <label for ="images1">Product Image (1):</label>
	<p> <input name="fileToUpload1" id="fileToUpload1" type="file" size="5"  class="er" required /></p>
  </div>
  <div class="field">
    <label for ="images2">Product Image (2):</label>
	<p> <input name="fileToUpload2" id="fileToUpload2" type="file" size="5"  class="er" required /></p>
  </div>
    <div class="field">
    <label for ="images3">Product Image (3):</label>
	<p> <input name="fileToUpload3" id="fileToUpload2" type="file" size="5"  class="er" required /></p>
  </div>
  <div class="" style=" margin-left:300px;">
    <button type="submit" class="btn btn-default" value="ADD"  name="ADD" style="">Submit</button>
	</div>
  </form>
 </div>
<?php

$t=time();
$time = date("Y-m-d",$t);

	
	if(isset($_POST['ADD']))
	{
		$productname = $_POST["productname"];
		$subcategory= $_POST["category"];
		$result4=mysqli_query($conn, "SELECT * FROM subcategory WHERE Subcategory_ID=$subcategory");
		$row4=mysqli_fetch_array($result4);
		$category = $row4['Category_ID'];
		$price = $_POST["price"];
		$discount = $_POST["discount"];
		//$prod_b = $_POST["prod_brand"];
		$detail = $_POST["detail"];
		$prod_qty = $_POST["prod_qty"];
		$prod_status = $_POST["prod_status"];
		$images1 = $_POST["fileToUpload1"];
		$images2 = $_POST["fileToUpload2"];
		$images3 = $_POST["fileToUpload3"];
		//echo $productname;
		//echo $subcategory;
		//echo $category;
		//echo $price ;
		//echo $discount;
		//echo $detail;
		//echo $feature;
		//echo $prod_qty;
		//echo $prod_status;
		//echo $images1;
		//echo $images2;
		//echo $images3 ; 
		if(mysqli_query($conn, "INSERT INTO `image`(`Images_1`, `Images_2`, `Images_3`) VALUES ('$images1','$images2','$images3')"))
		{
			$result5=mysqli_query($conn,"SELECT Images_ID FROM image WHERE Images_ID=(SELECT MAX(Images_ID) FROM image)"); 
			$row5=mysqli_fetch_array($result5);
			$nowid=$row5['Images_ID'];
		}
		if (mysqli_query($conn, "
		INSERT INTO `product`(`Product_Name`, `Product_Point`, `Product_Discount`, `Product_Description`, `Product_Quantity`, `Product_Status`, `Product_AddDate`, `Category_ID`, `Subcategory_ID`, `Images_ID`) 
		VALUES ('$productname', '$price', '$discount', '$detail', '$prod_qty','$prod_status','$time', '$category', '$subcategory', '$nowid')"))
		{
			?> <script text/javascript> alert("Product added Success");</script>
	<?php
		}
		else
		{
			echo "Can't Add product Please Retry!";
		}
		
	}
        
?>
<script>
 $('document').ready(function()
	  { 
	      
          $("#addproduct").validate({
		         errorPlacement: function (error, element) {
            error.insertAfter(element);
            if (element.hasClass('form-control')) {
                element.next().removeClass('passValid').addClass('passError');
            }
        },
		success: function (label) {
            if (label.prev().hasClass('form-control')) {
			   
			   label.text('ok!').addClass("checked");
			
            }
        },

			    rules: {
                    productname:{
					  required:true,
				      lettersonly:true
					  },
            
                    category: {
                        required: true,
						 valueNotEquals:"default"
                       },
                    producttype: {
                        required: true,
						 valueNotEquals:"default"
                    
                    },
					price: {
			               required:true,
                           price:true
                    },
					detail:{
					     required:true
						
					},
					feature:{
						required:true
				
					},
					images1:{
						required:true

					},
					images2:{
						required:true
		
				    },
					images3:{
						required:true
				
					},
				
                },
                messages: {
                    productname: { 
					    required:"Please Enter Product Name",
						lettersonly:"Please enter lettersonly"
				     },
	                category: {
                        required: "Please Choose Category",
                        valueNotEquals: "Please choose your state!"
                    },
					producttype:{
						    required:"Please Choose Product Type",
						    valueNotEquals: "Please choose your state!"
					},
                    price: {
						 required:"Please Enter Price",
						 price:"Invalid Format"
	                      },
				    detail: {
						 required:"Please Enter product description"
				
						},
					feature: {
						 required:"Please Enter your birthdate",
						 date:"invalid date"
				
						},
				   images1:{
					  required:"Please upload images"
					  },
				   images2:{
					  required:"Please upload images"
			
				  },
				  images3:{
					  required:"Please upload images"
				
				  },
				state:{
					  required:"Please choose your state",
					  valueNotEquals: "Please choose your state!"
					
				  },
                },
                submitHandler: function(form) {
                    form.submit();
					$( '#addproduct').each(function(){
                     this.reset();
					});
                }
			
            }); 
   	   
     jQuery.validator.addMethod("num", function(value, element) {
    return this.optional(element) || /^6?01\d{8}$/.test(value);
     }, "Letters only please"); 
     jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value);
     }, "");
	  jQuery.validator.addMethod("price", function(value, element) {
    return this.optional(element) || /^\d{0,4}(\.\d{0,2})?$/.test(value);
     }, "");
    jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
     return arg != value;
   }, "Value must not equal arg.");

	
});
                                    <div class="col-md-6 hidden-lg hidden-md text-center">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    Company
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="../assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/js/material.min.js" type="text/javascript"></script>
<!--  Charts Plugin -->
<script src="../assets/js/chartist.min.js"></script>
<!--  Dynamic Elements plugin -->
<script src="../assets/js/arrive.min.js"></script>
<!--  PerfectScrollbar Library -->
<script src="../assets/js/perfect-scrollbar.jquery.min.js"></script>
<!--  Notifications Plugin    -->
<script src="../assets/js/bootstrap-notify.js"></script>
<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<!-- Material Dashboard javascript methods -->
<script src="../assets/js/material-dashboard.js?v=1.2.0"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="../assets/js/demo.js"></script>

</html>