<?php include("db.php");?>
	
<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
  
  
  
      <style>
      /* NOTE: The styles were added inline because Prefixfree needs access to your styles and they must be inlined if they are on local disk! */
      @import url(https://fonts.googleapis.com/css?family=Exo:100,200,400);
@import url(https://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

body{
	margin: 0;
	padding: 0;
	background: #fff;

	color: #fff;
	font-family: Arial;
	font-size: 12px;
}

.body{
	position: absolute;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background-image: url(http://ginva.com/wp-content/uploads/2012/07/city-skyline-wallpapers-008.jpg);
	background-size: cover;
	-webkit-filter: blur(5px);
	z-index: 0;
}

.grad{
	position: absolute;
	top: -20px;
	left: -20px;
	right: -40px;
	bottom: -40px;
	width: auto;
	height: auto;
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
	z-index: 1;
	opacity: 0.7;
}

.header{
	position: absolute;
	top: calc(50% - 35px);
	left: calc(50% - 255px);
	z-index: 2;
}

.header div{
	float: left;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 35px;
	font-weight: 200;
}

.header div span{
	color: #5379fa !important;
}

.login{
	position: absolute;
	top: calc(50% - 75px);
	left: calc(50% - 50px);
	height: 150px;
	width: 350px;
	padding: 10px;
	z-index: 2;
}

.login input[type=text]{
	width: 250px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
}

.login input[type=password]{
	width: 250px;
	height: 30px;
	background: transparent;
	border: 1px solid rgba(255,255,255,0.6);
	border-radius: 2px;
	color: #fff;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 4px;
	margin-top: 10px;
}

.login input[type=button]{
	width: 260px;
	height: 35px;
	background: #fff;
	border: 1px solid #fff;
	cursor: pointer;
	border-radius: 2px;
	color: #a18d6c;
	font-family: 'Exo', sans-serif;
	font-size: 16px;
	font-weight: 400;
	padding: 6px;
	margin-top: 10px;
}

.login input[type=button]:hover{
	opacity: 0.8;
}

.login input[type=button]:active{
	opacity: 0.6;
}

.login input[type=text]:focus{
	outline: none;
	border: 1px solid rgba(255,255,255,0.9);
}

.login input[type=password]:focus{
	outline: none;
	border: 1px solid rgba(255,255,255,0.9);
}

.login input[type=button]:focus{
	outline: none;
}

::-webkit-input-placeholder{
   color: rgba(255,255,255,0.6);
}

::-moz-input-placeholder{
   color: rgba(255,255,255,0.6);
}
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

</head>

<body>

  <div class="body"></div>
		<div class="grad"></div>
		<div class="header">
			<div>Youde<span style="padding:4px; ">Admin</span></div>
		</div>
		<br>
		<div class="login">
		<form method="post" name="adminlogin">
				<input type="text" placeholder="username" name="username" id="username" required><br>
				<input type="password" placeholder="password" name="password" id="username" required><br>
				<input type="button" type="submit" value="login" name="login" onclick = "validate()">
		</form>
		</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

</body>

</html>
 <script type="text/javascript">

function validate()
{

//var msg = "";
//firstErrorField = null;
{ //for all cases
        
		username = document.adminLoginForm.Login_Name.value;
		password = document.adminLoginForm.Password.value;
		                          

   if (username === "" || password === "" )
	{
			alert("User ID and Password are required.");
			if (username === "" )
				document.adminLoginForm.Login_Name.focus;
			else if(password === "" )	
				document.adminLoginForm.Password.focus;
			return false;
	}
	else 
			
			return true;
    }
}//end of validate



</script>
<?php
error_reporting(0);
ini_set('display_errors', 0);
session_start();

 
if(isset($_POST['login']))
{

 $name=$_POST['username'];
 $pwd=$_POST['password'];
 $sql="select * from admin where Admin_Username='$name' and Admin_Password='$pwd'";
 $result = mysqli_query($con, $sql); 

 if($row = mysqli_fetch_assoc($result))
 {
	 
    $_SESSION["Sess_id"] = $row["Agent_Id"];
   $_SESSION["Admin_Username"] = $row["Admin_Username"];
    header("Location: dashboard.php?login=$row[Agent_Id]");
	//echo "<a href='product_detail.php?Login_Button=$row[Admin_ID]'>";
   }


 else if($name != "" && $pwd != "" )
   {
	   ?>
	   <script type="text/javascript">
			alert("You entered username or password is incorrect");
		</script>
	<?php
  }


  }

?>

