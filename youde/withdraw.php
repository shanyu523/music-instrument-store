<!DOCTYPE html>
<html lang="en">
<head>
  <title>E-wallet Transfer	</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
   <?php include_once("header.php");?>

  <section id="main-content">
  <section class="wrapper" style=" ">
  <div class="row">
  <div class="col-lg-12">
  <h3 class="page-header"><i class="fa fa-files-o"></i> E-wallet Withdraw</h3>

  </div>
</div>
 <div class="row">
  <div class="col-lg-12">
   
   <section class="panel">
   <div class="panel-body">
   <div class="form">
  <form action="/action_page.php"  class="form-validate form-horizontal" >

    <div class="form-group" >
	
      <label for="pwd" class="control-label col-lg-2">Current Amount<span class="required">*</span>:</label>
	  <div class="col-lg-10" style="width:280px;">
      <input type="password" class="form-control" id="to" placeholder="Enter Receive Name" name="pwd" style="">
	  </div>
    </div>
	<div class="form-group" >
      <label for="pwd" class="control-label col-lg-2">Amount<span class="required">*</span>:</label>
	  <div class="col-lg-10" style="width:280px;">
      <input type="password" class="form-control" id="balance" placeholder="Enter Balance" name="pwd" style="">
	  </div>
    </div>
	<div class="form-group" >
      <label for="pwd" class="control-label col-lg-2">Description<span class="required">*</span>:</label>
	  <div class="col-lg-10" style="width:280px;">
      <input type="password" class="form-control" id="description" placeholder="Enter Description" name="pwd" style="">
	  </div>
    </div>
	<div class="form-group" >
      <label for="pwd" class="control-label col-lg-2">E-Pin<span class="required">*</span>:</label>
	  <div class= "col-lg-10" style="width:280px;">
      <input type="password" class="form-control" id="description" placeholder="Enter E-pin" name="pwd" style="">
	  </div>
    </div>

    <div class="form-group">
	<div class="col-lg-offset-2 col-lg-10" >
      
       <button class="btn btn-default" type="button">Cancel</button>
	    <button class="btn btn-primary" type="submit" style="margin:12px;">Submit</button>
	</div>
	</div>
  </form>
</div>

</section>
</section>
</div>
</div>
      <?php include_once("footer.php");?> 
</body>
</html>



<?php
	
?>