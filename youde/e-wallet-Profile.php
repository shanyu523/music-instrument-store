<!DOCTYPE html>
<html lang="en">
<head>
  <title>E-wallet Transfer	</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  	<link type="text/css" href="css/bootstrap.min.css" rel="stylesheet">
		<link type="text/css" href="css/bootstrap-table.css" rel="stylesheet">
		<link type="text/css" href="css/font-awesome.css" rel="stylesheet">
</head>
</head>
<body>
   <?php include_once("header.php");?>

  <section id="main-content">
  <section class="wrapper" style=" ">
  <div class="row">
  <div class="col-lg-12">
  <h3 class="page-header"><i class="fa fa-files-o"></i> E-wallet Profile</h3>
    <ol class="breadcrumb">
              <li><i class="fa fa-home"></i>E-wallet History</li>
       
  </div>
</div>
`
   <div class="container">
   <section class="panel">
   <div class="panel-body">
   		<div class="row">
					<div class="col-md-12">
					 
						<table 	id="table"
			                	data-show-columns="true"
 				                data-height="460">
						</table>
					</div>
				</div>

</section>
</section>
</div>
</div>
      <?php include_once("footer.php");?> 
	  
	<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-table.js"></script>


<script type="text/javascript">
	
	 var $table = $('#table');
		     $table.bootstrapTable({
			      url: 'list-user.php',
			      search: true,
			      pagination: true,
			      buttonsClass: 'primary',
			      showFooter: true,
			      minimumCountColumns: 3,
			      columns: [{
			          field: 'num',
			          title: '#',
			          sortable: true,
			      },{
			          field: 'first',
			          title: 'Firstname',
			          sortable: true,
			      },{
			          field: 'product',
			          title: 'product name',
			          sortable: true,
			      },{
			          field: 'last',
			          title: 'Lastname',
			          sortable: true,
			          
			      },  ],
 
  			 });

</script>  
</body>
</html>



<?php
	
?>